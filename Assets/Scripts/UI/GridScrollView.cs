﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GridScrollView : MonoBehaviour
    {
        [Header("Params")] [SerializeField] private Vector2 baseSize = new Vector2(1440, 3040);
        [SerializeField] private bool toAdjustSpacing = true;
        [Header("Dep")] [SerializeField] private GridLayoutGroup layoutGroup;
        [SerializeField] private RectTransform content;
        private Vector2 _baseCellSize;
        private Vector2 _baseCellSpacing;

        private void Awake()
        {
            _baseCellSize = layoutGroup.cellSize;
            _baseCellSpacing = layoutGroup.spacing;
#if DEBUG
        }

        private void Update()
        {
#endif
            Vector2 screenSize = new Vector2(Screen.width, Screen.height); // Current screen size
            
            Debug.Log($"Current screen size:\t{screenSize}");
            
            layoutGroup.cellSize = (screenSize / baseSize) * _baseCellSize;
            layoutGroup.spacing = (screenSize / baseSize) * _baseCellSpacing;

            if (toAdjustSpacing)
            {
                if (layoutGroup.constraint == GridLayoutGroup.Constraint.FixedColumnCount)
                {
                    var temp = layoutGroup.spacing;
                    temp.x = CalculateSpacing(content.rect.width, layoutGroup.cellSize.x, layoutGroup.constraintCount);
                    layoutGroup.spacing = temp;
                }
                else if (layoutGroup.constraint == GridLayoutGroup.Constraint.FixedRowCount)
                {
                    var temp = layoutGroup.spacing;
                    temp.y = CalculateSpacing(content.rect.height, layoutGroup.cellSize.y, layoutGroup.constraintCount);
                    layoutGroup.spacing = temp;
                }
            }
        }

        private float CalculateSpacing(float totalLength, float cellLength, int cellNumber)
        {
            float remainedWidth = totalLength - (cellLength * cellNumber);
            return remainedWidth / (cellNumber + 1);
        }
    }
}