﻿using UnityEngine;

namespace UI.Panel
{
    public class Panel : MonoBehaviour
    {
        [Header("Parent Manager")] [SerializeField]
        private PanelManager panelManager;
        [Header("Self Dep")]
        [SerializeField] private GameObject next;
        [SerializeField] private GameObject from;
        
        public void Switch()
        {
            var from = this.from;
            if (from == null) 
                from = gameObject;
            panelManager.PushSwitch(from, next);
        }
    }
}