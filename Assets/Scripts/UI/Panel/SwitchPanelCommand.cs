﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Panel
{
    public class SwitchPanelCommand
    {
        private readonly GameObject _to;
        private readonly GameObject _from;

        public SwitchPanelCommand([NotNull] GameObject to, [NotNull] GameObject from)
        {
            if (to == null)
                throw new ArgumentNullException(nameof(to));
            if (from == null)
                throw new ArgumentNullException(nameof(from));
            _to = to;
            _from = from;
        }

        public void Begin()
        {
            _to.SetActive(true);
            _from.SetActive(false);
        }

        public void Rollback()
        {
            _from.SetActive(true);
            _to.SetActive(false);
        }

        public GameObject To => _to;

        public GameObject From => _from;
    }
}