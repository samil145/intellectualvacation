﻿using UnityEngine;
using System.Collections.Generic;

namespace UI.Panel
{
    public class PanelManager : MonoBehaviour
    {
        private readonly Stack<SwitchPanelCommand> _panelCommands = new Stack<SwitchPanelCommand>();
        
        public void Back()
        {
            if (_panelCommands.TryPop(out var command))
            {
                command.Rollback();

                Debug.Log(
                    $"[{nameof(PanelSwitchSimple)}] - Rollback from {command.To.name} to {command.From.name}" +
                    $"\nCommands count {_panelCommands.Count}");
            }
        }

        public void PushSwitch(GameObject from, GameObject to)
        {
            SwitchPanelCommand command = new SwitchPanelCommand(to, from);
            command.Begin();
            _panelCommands.Push(command);

            Debug.Log(
                $"[{nameof(PanelSwitchSimple)}] - Switched from {from.name} to {to.name}" +
                $"\nCommands count {_panelCommands.Count}");
        }
    }
}