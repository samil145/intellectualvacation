using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Panel
{
    public class PanelSwitchSimple : MonoBehaviour
    {
        public void SwitchPanel(GameObject go)
        {
            if (go != null)
            {
                go.SetActive(!go.activeSelf);
            }
        }

        public void TurnOnPanel(GameObject go)
        {
            if (go != null)
            {
                go.SetActive(true);
            }
        }

        public void TurnOffPanel(GameObject go)
        {
            if (go != null)
            {
                go.SetActive(false);
            }
        }
    }
}