﻿using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class RectResize : MonoBehaviour
    {
        private enum ResizeType
        {
            Width,
            Height,
            All
        }

        [Header("Params")] [SerializeField] private Vector2 baseScreenSize = new Vector2(1440, 3040);
        [SerializeField] private ResizeType resizeType = ResizeType.All;
        [SerializeField] private Vector2 baseSize = new Vector2(300, 180);
        private RectTransform _selfRectTransform;

#if DEBUG
        private void Update()
#else
        private void Awake()
#endif
        {
            _selfRectTransform = transform as RectTransform;
            
            Vector2 screenSize = new Vector2(Screen.width, Screen.height); // Current screen size

            var size = (screenSize / baseScreenSize) * baseSize;
            switch (resizeType)
            {
                case ResizeType.Width: _selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x); break;
                case ResizeType.Height: _selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y); break;
                case ResizeType.All: 
                    _selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x); 
                    _selfRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y); 
                    break;
            }
        }
    }
}