﻿using System;
using UnityEngine;

namespace UI
{
    public class SquareImageResize : MonoBehaviour
    {
        [SerializeField] private Vector2 baseSize = new Vector2(1440, 3040);

        private Vector2 _baseImageSize;

        private void Awake()
        {
            if (transform is RectTransform rectTransform)
            {
                _baseImageSize = rectTransform.sizeDelta;
#if DEBUG
            }
        }

        private void Update()
        {

            if (transform is RectTransform rectTransform)
            {
#endif
                Vector2 screenSize = new Vector2(Screen.width, Screen.height); // Current screen size
                Debug.Log(screenSize);
                var cellSize = (screenSize / baseSize) * _baseImageSize;

                var scale = (cellSize.x + cellSize.y) / 2f;

                rectTransform.sizeDelta = new Vector2(scale, scale);
            }
        }
    }
}