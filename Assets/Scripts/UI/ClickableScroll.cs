﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;
using System.Linq;
using Utils;

namespace UI
{
    public abstract class ClickableScroll<TData> : MonoBehaviour where TData : IId
    {
        [Header("Current Page Render")] [SerializeField]
        private Transform scrollViewContent;

        [FormerlySerializedAs("categoryContainerPrefab")] [SerializeField] private ClickableImage clickableImagePrefab;

        protected readonly IDictionary<ulong, ClickableImage> ClickableImages = new Dictionary<ulong, ClickableImage>();
        
        public void Populate(IEnumerable<TData> rawData)
        {
            foreach (var levelButton in ClickableImages.Select(x => x.Value))
            {
                levelButton.gameObject.SetActive(false);
            }

            foreach (var data in rawData)
            {
                if (ClickableImages.TryGetValue(data.ID, out var clickableImage))
                {
                    clickableImage.gameObject.SetActive(true);
                }
                else
                {
                    var createdContainer = Instantiate(clickableImagePrefab, scrollViewContent);

                    InitContainer(createdContainer, data);
                    
                    ClickableImages.Add(data.ID, createdContainer);
                }
            }
        }

        protected abstract void InitContainer(ClickableImage clickableImage, TData data);
    }
}