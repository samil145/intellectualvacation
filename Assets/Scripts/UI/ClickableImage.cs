﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Networking;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    //TODO: Make promise image
    public class ClickableImage : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TMPro.TMP_Text textHolder;
        [SerializeField] private Button button;
        [SerializeField] private bool toClearListenersAfterInit = true;
        private Networking.DTO.Image _rawImage;
        private UnityEvent<Sprite> _spriteLoaded;

        public void Init(string text, Sprite spriteImage, UnityAction buttonEvent)
        {
            this.textHolder.text = text;
            if (spriteImage != null)
                this.image.sprite = spriteImage;
            if (toClearListenersAfterInit)
                button.onClick.RemoveAllListeners();
            button.onClick.AddListener(buttonEvent);
        }

        //TODO: Look can be substituted for Task instead of void
        public void LateInit(string text, Networking.DTO.Image spriteImage, UnityAction buttonEvent)
        {
            _spriteLoaded = new UnityEvent<Sprite>();
            this.textHolder.text = text;
            if (toClearListenersAfterInit)
                button.onClick.RemoveAllListeners();
            button.onClick.AddListener(buttonEvent);
            _rawImage = spriteImage;
        }

        private async void Update()
        {
            if (_rawImage != null)
            {
                var temp = _rawImage;
                _rawImage = null;
                var imageLoader = NetworkManager.Instance.NetworkImageLoader;
                Sprite sprite = await imageLoader.GetImage(temp);
                _spriteLoaded.Invoke(sprite);
                this.image.sprite = sprite;
            }
        }
        
        public event UnityAction<Sprite> SpriteLoaded
        {
            add => _spriteLoaded.AddListener(value);
            remove => _spriteLoaded.RemoveListener(value);
        }
    }
}