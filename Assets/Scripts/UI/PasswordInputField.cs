using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PasswordInputField : InputField
    {
        private bool _isPasswordHidden;

        protected override void Awake()
        {
            base.Awake();
            _isPasswordHidden = true;
            inputField.contentType = TMP_InputField.ContentType.Password;
        }

        public void ShowPassword()
        {
            _isPasswordHidden = !_isPasswordHidden;
            inputField.contentType = _isPasswordHidden
                ? TMP_InputField.ContentType.Password
                : TMP_InputField.ContentType.Standard;
            
            inputField.DeactivateInputField();
            inputField.ActivateInputField();
        }
    }
}