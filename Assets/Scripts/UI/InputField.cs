using TMPro;
using UnityEngine;

namespace UI
{
    public class InputField : MonoBehaviour
    {
        [Header("Dep")]
        [SerializeField] protected TMP_InputField inputField;
        [SerializeField] private GameObject errorGO;
        [SerializeField] private TMPro.TMP_Text errorText;

        [Header("Properties")] [SerializeField]
        private Color errorImageColor;
        [SerializeField] private Color errorInputTextColor;

        private Color _initialImageColor;
        private Color _initialTextColor;
        
        protected virtual void Awake()
        {
            errorGO.SetActive(false);
            _initialImageColor = inputField.image.color;
            _initialTextColor = inputField.textComponent.color;
            inputField.onDeselect.AddListener((str) => UpdateState());
            inputField.onValueChanged.AddListener((str) => UpdateState());
        }

        public void UpdateState(string error = null)
        {
            if (!string.IsNullOrEmpty(error))
            {
                errorGO.SetActive(true);
                inputField.image.color = errorImageColor;
                inputField.textComponent.color = errorInputTextColor;
                errorText.text = error;
            }
            else
            {
                errorGO.SetActive(false);
                inputField.image.color = _initialImageColor;
                inputField.textComponent.color = _initialTextColor;
            }
        }
        
        public string Text
        {
            get => inputField.text;
            set => inputField.text = value;
        }
    }
}

