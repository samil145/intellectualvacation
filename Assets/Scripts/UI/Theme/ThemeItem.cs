﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Theme
{
    [RequireComponent(typeof(Image))]
    public class ThemeItem : MonoBehaviour
    {
        [Serializable]
        private class Pair
        {
            public string theme;
            public Color color;
        }

        [SerializeField] private List<Pair> themes;
        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
        }

        public void SwitchTheme(string theme)
        {
            if (_image != null)
            {
                var themePair = themes.FirstOrDefault(x => x.theme == theme);
                if (themePair != null)
                {
                    _image.color = themePair.color;
                }
                else
                {
                    Debug.LogError($"Theme {theme} not found!");
                }
            }
        }
    }
}