﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI.Theme
{
    public class ThemeManager : MonoBehaviour
    {
        private const string DARK = "Dark";
        private const string LIGHT = "Light";

        [SerializeField] private List<ThemeItem> themeItems;
        [SerializeField] private List<string> mapper;

        public void SwitchTheme(int index)
        {
            if (mapper.Count <= index)
            {
                Debug.LogError($"No such index {index}", this);
                return;
            }
            SwitchTheme(mapper[index]);
        }
        
        public void SwitchTheme(string theme)
        {
            themeItems.ForEach(x => x.SwitchTheme(theme));
        }

        public void SwitchToDarkOrLight(bool toDark)
        {
            SwitchTheme(toDark ? DARK : LIGHT);
        }
    }
}