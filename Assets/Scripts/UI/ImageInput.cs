﻿using System;
using System.Collections.Concurrent;
using System.IO;
using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Button))]
    public class ImageInput : MonoBehaviour
    {
        public class TextureLoadedEventArgs
        {
            private Texture2D _texture2D;
            private byte[] _bytes;
            private string _extension;

            public TextureLoadedEventArgs(Texture2D texture2D, byte[] bytes, string extension)
            {
                _texture2D = texture2D;
                _bytes = bytes;
                _extension = extension;
            }

            public Texture2D Texture2D => _texture2D;

            public byte[] Bytes => _bytes;

            public string Extension => _extension;
        }

        private UnityEvent<TextureLoadedEventArgs> _textureLoaded;
        private Button _button;

        private void Awake()
        {
            if (_textureLoaded == null)
                _textureLoaded = new UnityEvent<TextureLoadedEventArgs>();
            _button = GetComponent<Button>();
            _button.onClick.AddListener(PickImage);
        }

        public event UnityAction<TextureLoadedEventArgs> TextureLoaded
        {
            add
            {
                if (_textureLoaded == null)
                    _textureLoaded = new UnityEvent<TextureLoadedEventArgs>();
                _textureLoaded.AddListener(value);
            }
            remove => _textureLoaded.RemoveListener(value);
        }

        private void PickImage()
        {
            NativeGallery.Permission permission = NativeGallery.GetImageFromGallery(LoadImageAsync);

            Debug.Log("Permission result: " + permission);
        }

        private async void LoadImageAsync(string path)
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                byte[] rawBytes = await File.ReadAllBytesAsync(path);
                //Texture2D texture = await NativeGallery.LoadImageAtPathAsync(path);
                string extension = Path.GetExtension(path).ToLowerInvariant();
                TextureFormat format = (extension == ".jpg" || extension == ".jpeg")
                    ? TextureFormat.RGB24
                    : TextureFormat.RGBA32;
                Texture2D texture = new Texture2D(2, 2, format, true, false);

                texture.LoadImage(rawBytes, true);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                }

                _textureLoaded.Invoke(new TextureLoadedEventArgs(texture, rawBytes, extension));
            }
        }
    }
}