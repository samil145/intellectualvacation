using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class StateButton : MonoBehaviour
{
    private Button _button;
    private TMPro.TMP_Text _text;
    private bool isSelected;

    private Color _baseImageColor;
    private Color _baseTextColor;
    [SerializeField] private Color selectedColorImage;
    [SerializeField] private Color selectedColorText;
    
    private void Awake()
    {
        isSelected = false;
        _button = GetComponent<Button>();
        _text = GetComponentInChildren<TMPro.TMP_Text>();

        _button.onClick.AddListener(ChangeState);
        
        _baseImageColor = _button.image.color;
        if (_text != null)
        {
            _baseTextColor = _text.color;
        }
    }

    private void ChangeState()
    {
        isSelected = !isSelected;
        if (isSelected)
        {
            _button.image.color = selectedColorImage;
            _text.color = selectedColorText;
        }
        else
        {
            _button.image.color = _baseImageColor;
            _text.color = _baseTextColor;
        }
    }

    public bool IsSelected
    {
        get => isSelected;
        set
        {
            isSelected = value;
            ChangeState();
        }
    }
}
