﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Toggle))]
    public class SliderToggle : MonoBehaviour
    {
        [Header("Dep")] [SerializeField] private RectTransform handle;
        [SerializeField] private Image handleImage;
        [SerializeField] private Image backgroundImage;
        [Header("Properties")] [SerializeField]
        private float speed;

        [SerializeField] private Color onHandleColor;
        [SerializeField] private Color onBackgroundColor;

        private Vector2 _initialPos;
        private Vector2 _targetPos;
        private Toggle _toggle;
        private Color _baseHandleColor;
        private Color _baseBackgroundColor;
        private Color _targetHandleColor;
        private Color _targetBackgroundColor;
        
        private void Awake()
        {
            _toggle = GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener(OnSwitch);
            _targetPos = _initialPos = handle.localPosition;
            _baseHandleColor = handleImage.color;
            _baseBackgroundColor = backgroundImage.color;
            OnSwitch(_toggle.isOn);
        }

        private void OnSwitch(bool flag)
        {
            var offPoss = _initialPos;
            offPoss.x *= -1;
            
            if (flag)
            {
                _targetPos = offPoss;
                _targetHandleColor = onHandleColor;
                _targetBackgroundColor = onBackgroundColor;
            }
            else
            {
                _targetPos = _initialPos;
                _targetHandleColor = _baseHandleColor;
                _targetBackgroundColor = _baseBackgroundColor;
            }
        }

        private void Update()
        {
            var clamped = Mathf.Clamp(speed * Time.deltaTime, 0f, 1f);
            
            handle.localPosition = Vector3.Lerp(handle.localPosition, _targetPos, clamped);
            handleImage.color = Color.Lerp(handleImage.color, _targetHandleColor, clamped);
            backgroundImage.color = Color.Lerp(backgroundImage.color, _targetBackgroundColor, clamped);
        }
    }
}