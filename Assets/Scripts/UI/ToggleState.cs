using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ToggleState : MonoBehaviour
    {
        [SerializeField] private Color backColorEnabled;
        [SerializeField] private Color textColorEnabled;
        [SerializeField] private Color toggleColorEnabled;
        [SerializeField] private Image backImage;
        [SerializeField] private TMPro.TMP_Text text;
        [SerializeField] private Image toggle;
        [SerializeField] private Color backColorDisEnabled;
        [SerializeField] private Color textColorDisEnabled;
        [SerializeField] private Color toggleColorDisEnabled;

        private void Start()
        {
            SetState(false);
        }

        public void SetState(bool isOn)
        {
            if (isOn)
            {
                backImage.color = backColorEnabled;
                text.color = textColorEnabled;
                toggle.color = toggleColorEnabled;
            }
            else
            {
                backImage.color = backColorDisEnabled;
                text.color = textColorDisEnabled;
                toggle.color = toggleColorDisEnabled;
            }
        }
    }
}