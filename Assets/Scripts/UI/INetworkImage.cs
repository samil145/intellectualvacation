﻿using Networking.DTO;

namespace UI
{
    public interface INetworkImage
    {
        Image NetworkImage { get; }
    }
}