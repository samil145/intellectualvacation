﻿using System;
using System.Collections.Generic;
using Networking.DTO.Story;
using UI;
using UI.Panel;
using UnityEngine;
using UnityEngine.UI;

namespace Story
{
    public class LevelHandler : MonoBehaviour
    {
        [Header("Images")] [SerializeField] private Image background;
        [SerializeField] private Image character;
        [Header("Progress")] [SerializeField] private Slider progress;
        [SerializeField] private float progressHandleSpeed = 5f;
        [Header("Middle")] [SerializeField] private GameObject middleTextHolder;
        [SerializeField] private TMPro.TMP_Text middleText;
        [Header("Bottom")] [SerializeField] private GameObject bottomHolder;
        [SerializeField] private TMPro.TMP_Text bottomText;
        [Header("Answers")] [SerializeField] private GameObject answersView;
        [SerializeField] private Transform answersContent;
        [SerializeField] private ClickableImage answerPrefab;
        [SerializeField] private Color defaultColor;
        [SerializeField] private Color correctColor;
        [SerializeField] private Color wrongColor;
        [Header("Question")] [SerializeField] private GameObject questionHolder;
        [SerializeField] private TMPro.TMP_Text questionText;
        [Header("Panels")] [SerializeField] private PanelManager panelManager;

        [Header("Gameplay Special Params")] [Min(1), SerializeField]
        private int maxNumberOfErrors = 3;

        [Min(0), SerializeField] private float switchItemTime = 1f;

        private Level _currentLevel;
        private int _currentItemIndex;
        private readonly List<ClickableImage> _answers = new List<ClickableImage>();
        private int _numberOfErrors;

        private void Update()
        {
            float target = Mathf.Clamp((float)_currentItemIndex / _currentLevel.items.Count, 0f, 1f);
            progress.value = Mathf.Lerp(progress.value, target, Time.deltaTime * progressHandleSpeed);
        }

        public void StartLevel(Level level)
        {
            _currentLevel = level;
            _currentItemIndex = -1;
            RenderNextItem();
        }

        private void RestartLevel()
        {
            StartLevel(_currentLevel);
        }

        public void RenderNextItemAfterSomeTime()
        {
            Invoke(nameof(RenderNextItem), switchItemTime);
        }

        public void RenderNextItem()
        {
            // Characterless text (Middle)
            // Character text (Bottom)
            // Questions (Bottom and small up)

            ++_currentItemIndex;

            if (_currentItemIndex >= _currentLevel.items.Count)
            {
                panelManager.Back();
                progress.value = 0;
                _currentItemIndex = -1;
                Debug.Log("[Story] - Game over");
                return;
            }

            Debug.Log($"[Story] - Current Index:\t{_currentItemIndex}");

            var item = _currentLevel.items[_currentItemIndex];

            //character.sprite = item.CharacterImage;
            SetSpriteIntoImage(character, item.CharacterImage);
            character.gameObject.SetActive(item.CharacterImage != null);

            SetSpriteIntoImage(background, item.LocationImage);

            if (item.storyAnswers == null || item.storyAnswers.Count == 0)
            {
                if (item.CharacterImage == null)
                {
                    Switch2CharacterlessState(item);
                }
                else
                {
                    Switch2CharacterState(item);
                }
            }
            else
            {
                Switch2AnswersState(item);
            }
        }

        private void SetSpriteIntoImage(Image image, Sprite sprite)
        {
            image.sprite = sprite;
            if (sprite != null)
            {
                var texture = sprite.texture;
                Vector2 screenSize = new Vector2(Screen.width, Screen.height); // Current screen size
                var rectTransform = image.rectTransform;
                Vector2 textureSize = new Vector2(texture.width, texture.height);
                float k = screenSize.y / textureSize.y;
                rectTransform.sizeDelta = new Vector2(textureSize.x * k, screenSize.y);
            }
        }

        private void Switch2AnswersState(LevelItem levelItem)
        {
            bottomHolder.SetActive(true);
            questionHolder.SetActive(true);
            middleTextHolder.SetActive(false);

            questionText.text = levelItem.text;

            answersView.SetActive(true);
            bottomText.gameObject.SetActive(false);

            SwapAnswers(levelItem.storyAnswers);
        }

        private void Switch2CharacterlessState(LevelItem levelItem)
        {
            bottomHolder.SetActive(false);
            questionHolder.SetActive(false);
            middleTextHolder.SetActive(true);

            middleText.text = levelItem.text;
        }

        private void Switch2CharacterState(LevelItem levelItem)
        {
            bottomHolder.SetActive(true);
            questionHolder.SetActive(false);
            middleTextHolder.SetActive(false);

            answersView.SetActive(false);
            bottomText.gameObject.SetActive(true);
            bottomText.text = levelItem.text;
        }

        private void SwapAnswers(List<Answer> answers)
        {
            var containersCount = answersContent.childCount;

            for (int j = 0; j < answers.Count - containersCount; j++)
            {
                var container = Instantiate(answerPrefab, answersContent);
                container.gameObject.SetActive(false);
                _answers.Add(container);
            }

            int i = 0;
            for (; i < answers.Count; i++)
            {
                var answerData = answers[i];
                var container = _answers[i];
                var image = container.GetComponent<Image>();
                image.color = defaultColor;

                container.Init(answerData.text, null, () =>
                {
                    if (!answerData.isCorrect) ++_numberOfErrors;
                    if (_numberOfErrors >= maxNumberOfErrors)
                    {
                        Invoke(nameof(RestartLevel), switchItemTime);
                    }
                    else
                    {
                        Invoke(nameof(RenderNextItem), switchItemTime);
                    }

                    image.color = answerData.isCorrect ? correctColor : wrongColor;
                });
                container.gameObject.SetActive(true);
            }

            for (; i < _answers.Count; ++i)
            {
                var container = _answers[i];
                container.gameObject.SetActive(false);
            }
        }
    }
}