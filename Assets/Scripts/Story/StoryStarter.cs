﻿using UnityEngine;
using UnityEngine.UI;

namespace Story
{
    public class StoryStarter : MonoBehaviour
    {
        [SerializeField] private Image mainImage;
        [SerializeField] private TMPro.TMP_Text description;
        [SerializeField] private TMPro.TMP_Text storyName;
        private Networking.DTO.Story.Story _story;
        
        public void Init(Networking.DTO.Story.Story rawData)
        {
            _story = rawData;
            mainImage.sprite = rawData.Sprite;
            description.text = rawData.description;
            storyName.text = rawData.name;
        }

        public ulong SpriteId => _story.id;

        public bool HasStory => _story != null;
    }
}