﻿using UnityEngine;

namespace Networking.View.Story
{
    public class StoryDescription : MonoBehaviour
    {
        [SerializeField] private TMPro.TMP_Text description;

        public void SetDescription(string text)
        {
            description.text = text;
        }
    }
}