﻿using IO;
using Networking.DTO.Story;
using UnityEngine;
using UnityEngine.UI;

namespace Networking.View.Story
{
    public class LevelCost : MonoBehaviour
    {
        [Header("Dep")]
        [SerializeField] private Image image;
        [SerializeField] private TMPro.TMP_Text costText;
        [SerializeField] private GameObject costGO;
        [Header("Properties")] [SerializeField]
        private Color inaccessibleColor;

        private bool _isUnlocked;
        
        public void Init(Level level)
        {
            _isUnlocked = level.isPurchased;
            var user = LocalStorage.Instance.Get<DTO.User.Response.User>("user");
            costGO.SetActive(!level.isPurchased);
            if (!level.isPurchased)
            {
                if (user.stars < level.cost)
                {
                    image.color = inaccessibleColor;
                }

                costText.text = $"-{level.cost}";
            }
        }

        public bool IsUnlocked => _isUnlocked;
    }
}