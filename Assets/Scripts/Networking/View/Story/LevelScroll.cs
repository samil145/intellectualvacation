﻿using System;
using System.Threading.Tasks;
using IO;
using Networking.DTO.Story;
using Request = Networking.DTO.Story.Request;
using Networking.WebService.Story;
using Story;
using UI;
using UI.Panel;
using UnityEngine;

namespace Networking.View.Story
{
    public class LevelScroll : ClickableScroll<Level>
    {
        private IStoryWebService _storyWebService;
        [Header("Deps")] [SerializeField] private LevelHandler levelHandler;
        [SerializeField] private GameObject errorMessageBox;
        [SerializeField] private GameObject notEnoughStarsMessageBox;
        [Header("Panels")] [SerializeField] private PanelManager panelManager;
        [SerializeField] private GameObject currentPanel;
        [SerializeField] private GameObject nextPanel;
        private bool _isReadyToBuy = false;

        private void Start()
        {
            _storyWebService = NetworkManager.Instance.Config.GetBean<IStoryWebService>();
        }

        protected override void InitContainer(ClickableImage clickableImage, Level data)
        {
            clickableImage.Init($"Level {data.Order}", null, async () =>
            {
                await OnButtonClicked(data);
            });
            
            var levelCost = clickableImage.GetComponent<LevelCost>();
            levelCost.Init(data);
        }

        private async Task OnButtonClicked(Level rawData)
        {
            var levelCost = ClickableImages[rawData.ID].GetComponent<LevelCost>();
            if (levelCost.IsUnlocked)
            {
                var resp = await _storyWebService.GetLevel(rawData.id);
                if (resp.IsSuccessful)
                {
                    //TODO: Make it more normal 
                    levelHandler.StartLevel(new Level()
                    {
                        items = resp.Result
                    });
                    panelManager.PushSwitch(currentPanel, nextPanel);
                }
            }
            else
            {
                var user = LocalStorage.Instance.Get<DTO.User.Response.User>("user");
                var hasEnoughStars = user.stars >= rawData.cost;
                if (hasEnoughStars)
                {
                    var resp = await _storyWebService.BuyLevel(new Request.BuyLevel()
                    {
                        id = rawData.id
                    });
                    if (resp.IsSuccessful)
                    {
                        levelCost.Init(resp.Result);
                    }

                    errorMessageBox.SetActive(!resp.IsSuccessful);
                }

                notEnoughStarsMessageBox.SetActive(!hasEnoughStars);
            }
        }
    }
}