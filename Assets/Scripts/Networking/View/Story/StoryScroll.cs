﻿using System.Linq;
using System.Threading.Tasks;
using Networking.WebService.Story;
using Story;
using UI.Panel;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Networking.View.Story
{
    public class StoryScroll : ClickableScroll<DTO.Story.Story>
    {
        [Header("Global Deps")] [SerializeField]
        private LevelScroll levels;

        [SerializeField] private Image bg2;

        [Header("Self Deps")] [SerializeField] private Image image;
        [SerializeField] private Button button;
        [SerializeField] private TMPro.TMP_Text text;
        [SerializeField] private StoryStarter storyStarter;

        [Header("Panel switch")] [SerializeField]
        private GameObject currentPage;

        [SerializeField] private GameObject nextPage;
        [Header("Scope Dep")] [SerializeField] private PanelManager panelManager;

        private DTO.Story.Story _story;

        private IStoryWebService _storyWebService;

        private void Start()
        {
            _storyWebService = NetworkManager.Instance.Config.GetBean<IStoryWebService>();
        }

        private void Awake()
        {
            button.onClick.AddListener(Begin);
        }

        private async void Begin()
        {
            NetworkManager.Instance.Logger.Log($"Story {_story.id} is selected.", LogType.Log);
            var req = new DTO.Story.Request.Levels();
            req.id = _story.id;

            NetworkManager.Instance.Logger.Log("[Web Request] - GetLevels", LogType.Log);

            var resp = await _storyWebService.GetLevels(req);
            var bgImage = await NetworkManager.Instance.NetworkImageLoader.GetImage(_story.levelsBgImage);

            if (resp.IsSuccessful)
            {
                int index = 1;
                SetSpriteIntoImage(bg2, bgImage);
                bg2.gameObject.SetActive(true);
                levels.Populate(resp.Result.Select(level =>
                {
                    level.Order = index++;
                    return level;
                }));
            }

            storyStarter.gameObject.SetActive(false);

            panelManager.PushSwitch(currentPage, nextPage);
        }
        
        private void SetSpriteIntoImage(Image image, Sprite sprite)
        {
            image.sprite = sprite;
            if (sprite != null)
            {
                var texture = sprite.texture;
                Vector2 screenSize = new Vector2(Screen.width, Screen.height); // Current screen size
                var rectTransform = image.rectTransform;
                Vector2 textureSize = new Vector2(texture.width, texture.height);
                float k = screenSize.y / textureSize.y;
                rectTransform.sizeDelta = new Vector2(textureSize.x * k, screenSize.y);
            }
        }


        protected override void InitContainer(ClickableImage clickableImage, DTO.Story.Story data)
        {
            clickableImage.LateInit(data.name, data.NetworkImage, () => { OnButtonClicked(data); });
            clickableImage.SpriteLoaded += arg0 =>
            {
                data.Sprite = arg0;
                if (storyStarter.HasStory && storyStarter.SpriteId == data.id)
                {
                    storyStarter.Init(data);
                }
            };
            if (clickableImage.TryGetComponent(out StoryDescription storyDescription))
            {
                storyDescription.SetDescription(data.description);
            }
        }

        private void OnButtonClicked(DTO.Story.Story rawData)
        {
            storyStarter.gameObject.SetActive(true);
            storyStarter.Init(rawData);
            if (rawData.Sprite != null)
                image.sprite = rawData.Sprite;
            this.text.text = rawData.description;
            _story = rawData;
        }
    }
}