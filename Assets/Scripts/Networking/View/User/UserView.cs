﻿using System;
using System.Threading.Tasks;
using IO;
using Networking.DTO;
using Networking.DTO.User.Request;
using Networking.WebService.User;
using UI;
using UnityEngine;
using UnityEngine.UI;
using static Utils.TextureUtils;

namespace Networking.View.User
{
    public class UserView : Component
    {
        [Header("Photo")] [SerializeField] private ImageInput photoInput;
        [SerializeField] private UnityEngine.UI.Image photoImage;
        [SerializeField] private Vector2Int imageSize;

        [Header("Menu Switch")] [SerializeField]
        private GameObject currentPage;

        [SerializeField] private GameObject nextPage;

        [Header("Render User")] [SerializeField]
        private TMPro.TMP_Text stars;

        [SerializeField] private TMPro.TMP_Text userName;
        [SerializeField] private TMPro.TMP_Text email;
        [SerializeField] private ImageInput photoInputAfterReg;
        [SerializeField] private UnityEngine.UI.Image photoImageAfterReg;

        private byte[] _chosenPhotoBytes;
        private string _fileExtension;

        private IUserWebService _userWebService;

        protected override void Init()
        {
            _userWebService = GetBean<IUserWebService>();
            photoInput.TextureLoaded += OnPictureChosen;
            photoInputAfterReg.TextureLoaded += OnPictureChosen;
        }

        private void OnPictureChosen(ImageInput.TextureLoadedEventArgs data)
        {
            Texture2D texture2D = data.Texture2D;
            _fileExtension = data.Extension;
            _chosenPhotoBytes = data.Bytes;

            var previous = texture2D;
            texture2D = texture2D.DuplicateTexture();
            Destroy(previous);

            var width = texture2D.width;
            var height = texture2D.height;

            Rect bounds = new Rect(
                0f,
                0f,
                Mathf.Min(imageSize.x, width),
                Mathf.Min(imageSize.y, height)
            );
            RenderPhotoImage(photoImage, texture2D, bounds);
            RenderPhotoImage(photoImageAfterReg, texture2D, bounds);
        }

        private void RenderPhotoImage(UnityEngine.UI.Image image, Texture2D texture2D, Rect rect)
        {
            if (image.sprite != null)
            {
                if (image.sprite.texture != null)
                    Destroy(image.sprite.texture);
                Destroy(image.sprite);
            }

            Vector2 pivot = 0.5f * Vector2.one;
            Sprite sprite = Sprite.Create(texture2D, rect, pivot);
            image.sprite = sprite;
        }

        public async void SetPhoto()
        {
            var data = new SetPhoto();
            data.file = _chosenPhotoBytes;
            data.fileExtension = _fileExtension;

            Log("SetPhoto", LogType.Log);

            var resp = await _userWebService.SetPhoto(data);

            //TODO: Decide later
            // if (resp.IsSuccessful)
            // {
            //     currentPage.SetActive(false);
            //     nextPage.SetActive(true);
            // }

            currentPage.SetActive(false);
            nextPage.SetActive(true);
        }

        public async Task GetUser()
        {
            var data = new DTO.User.Request.GetUser();

            Log("[Web Request] - Get User", LogType.Log);

            var resp = await _userWebService.GetUser(data);

            if (resp.IsSuccessful)
            {
                LocalStorage.Instance.Set("user", resp.Result);
            }

            Log("Render User", LogType.Log);

            if (resp.IsSuccessful)
            {
                RenderUser(resp.Result);
            }
        }

        public void RenderUser(DTO.User.Response.User user)
        {
            stars.text = user.stars.ToString();
        }
    }
}