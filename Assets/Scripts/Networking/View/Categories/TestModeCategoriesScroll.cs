﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Networking.View.Multiplayer;
using Networking.DTO.Categories;
using Networking.DTO.Multiplayer;
using UnityEngine;
using UI;
using UI.Panel;

namespace Networking.View.Categories
{
    public class TestModeCategoriesScroll : ClickableScroll<Category>
    {
        [Header("Panels")] [SerializeField] private GameObject from;
        [SerializeField] private GameObject to;
        [SerializeField] private PanelManager panelManager;
        [SerializeField] private QuizMultiplayer quizMultiplayer;

        protected override void InitContainer(ClickableImage clickableImage, Category data)
        {
            clickableImage.LateInit(data.name, data.NetworkImage, async () 
                => await quizMultiplayer.Begin(new ConnectToLobbyRequest() { categoryId = data.ID }));
        }

        public void SelectMultiplayer()
        {
            quizMultiplayer.IsMultiplayer = true;
            panelManager.PushSwitch(from, to);
        }

        public void SelectSinglePlayer()
        {
            quizMultiplayer.IsMultiplayer = false;
            panelManager.PushSwitch(from, to);
        }
    }
}