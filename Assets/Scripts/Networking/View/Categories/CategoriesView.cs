﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IO;
using Networking.DTO.Categories;
using Networking.View.Story;
using Networking.WebService.Categories;
using Networking.WebService.Story;
using Response = Networking.DTO.Categories.Response;
using Request = Networking.DTO.Categories.Request;
using UnityEngine;
using UI;
using UI.Panel;

namespace Networking.View.Categories
{
    public class CategoriesView : Component
    {
        [Header("Scrolls")] [SerializeField] private SelectCategoriesScroll categoriesScroll;
        [SerializeField] private StoryScroll specialStories;

        [Header("Panel Switch")] [SerializeField]
        private PanelManager panelManager;

        [SerializeField] private GameObject currentPage;

        [SerializeField] private GameObject nextPage;

        [Header("On populate handlers")] [SerializeField]
        private List<ClickableScroll<Category>> scrolls;

        private ICategoriesWebService _categoriesWebService;
        private IStoryWebService _storyWebService;

        protected override void Init()
        {
            _categoriesWebService = GetBean<ICategoriesWebService>();
            _storyWebService = GetBean<IStoryWebService>();
        }

        public async void GetAllCategoriesEventHandler()
        {
            await GetAllCategories();
        }

        public async Task GetAllCategories()
        {
            Log("[Web Request] - GetCategories", LogType.Log);

            var resp = await _categoriesWebService.Get();

            Log("RenderCategories", LogType.Log);
            if (resp.IsSuccessful)
            {
                scrolls.ForEach(x => x.Populate(resp.Result));
            }
        }

        public async void GetInterestingStoriesEventHandler()
        {
            await GetInterestingStories();
        }
        
        public async Task GetInterestingStories()
        {
            var list = SaveLoader.Instance.ListOfInterest;
            if (list == null)
            {
                Log("Interests found to be NULL", LogType.Error);
            }

            var req = new DTO.Story.Request.StoriesForYou();
            req.idsOfInterests = list;

            Log("[Web Request] - GetStoriesForYou", LogType.Log);

            var resp = await _storyWebService.GetStoriesForYou(req);

            if (resp.IsSuccessful)
            {
                specialStories.Populate(resp.Result);
            }
        }

        public async void SetMyInterests()
        {
            var selectedCategories = categoriesScroll.GetSelectedCategories();

            var data = new Request.SetMyInterests();
            data.categories = selectedCategories;

            Log($"TryGetMyInterestsData\tInterest ids: {{{data}}}", LogType.Log);

            var resp = await _categoriesWebService.SetMyInterests(data);

            Log("RenderSetMyInterest", LogType.Log);
            if (resp.IsSuccessful)
            {
                await GetInterestingStories();

                panelManager.PushSwitch(currentPage, nextPage);
            }
        }
    }
}