﻿using System.Threading.Tasks;
using Networking.DTO.Categories;
using Networking.View.Story;
using Networking.WebService.Story;
using UnityEngine;
using UI.Panel;
using UI;
using UnityEngine.Serialization;
using Request = Networking.DTO.Story.Request;
using Response = Networking.DTO.Story.Response;

namespace Networking.View.Categories
{
    public class AllCategoriesScroll : ClickableScroll<Category>
    {
        [Header("Panels")] [SerializeField] private GameObject from;
        [SerializeField] private GameObject to;
        [Header("Scope Dep")] [SerializeField] private PanelManager panelManager;

        [Header("Scrolls")]
        [SerializeField] private StoryScroll stories;
        
        private IStoryWebService _storyWebService;

        private void Start()
        {
            _storyWebService = NetworkManager.Instance.Config.GetBean<IStoryWebService>();
        }

        protected override void InitContainer(ClickableImage clickableImage, Category data)
        {
            clickableImage.LateInit(data.name, data.NetworkImage, async () =>
            {
                await OnButtonClicked(data);
            });
        }

        private async Task OnButtonClicked(Category rawData)
        {
            Request.StoriesAll req = new Request.StoriesAll()
            {
                id = rawData.id,
            };

            NetworkManager.Instance.Logger.Log("[Web Request] - GetStoriesByCategoryId", LogType.Log);
            
            var resp = await _storyWebService.GetStoriesByCategoryId(req);
            NetworkManager.Instance.Logger.Log($"[Web Response] - Result: {resp.Result}", LogType.Log);
            
            if (resp.IsSuccessful)
            {
                this.stories.Populate(resp.Result);
            }

            panelManager.PushSwitch(from, to);
        }
    }
}