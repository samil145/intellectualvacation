﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Networking.DTO.Categories;
using UI;
using UnityEngine;

namespace Networking.View.Categories
{
    public class SelectCategoriesScroll : ClickableScroll<Category>
    {
        private readonly ISet<ulong> _selectedCategories = new HashSet<ulong>();

        protected override void InitContainer(ClickableImage clickableImage, Category data)
        {
            clickableImage.LateInit(data.name, data.NetworkImage, () =>
            {
                OnButtonClicked(data);
            });
        }

        private void OnButtonClicked(Category rawData)
        {
            if (!_selectedCategories.Add(rawData.id))
            {
                _selectedCategories.Remove(rawData.id);
            }
        }

        public List<ulong> GetSelectedCategories() => _selectedCategories.ToList();
    }
}