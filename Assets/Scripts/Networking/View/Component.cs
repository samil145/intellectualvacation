﻿using UnityEngine;

namespace Networking.View
{
    public abstract class Component : MonoBehaviour
    {
        private NetworkManager _networkManager;
        private string _name;

        private void Start()
        {
            _name = GetType().Name;
            _networkManager = NetworkManager.Instance;

            Init();
        }

        protected virtual void Init()
        {
        }

        protected T GetBean<T>()
        {
            return _networkManager.Config.GetBean<T>();
        }
        
        protected void Log(object message, LogType logType)
        {
            _networkManager.Logger.Log($"[{_name}] - {message}", logType);
        }
    }
}