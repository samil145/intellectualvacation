﻿using Request = Networking.DTO.Account.Request;
using System;
using System.Text.RegularExpressions;
using Networking.WebService.Account;
using UnityEngine;
using UI;

namespace Networking.View.Account
{
    public class AccountView : Component
    {
        private const string PASWORD_PATTERN = @"^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$";

        [Serializable]
        private class LogInDependencies
        {
            [Header("Data to send")] public InputField email;
            public InputField password;
            [Header("Render")] public GameObject currentPage;
            public GameObject nextPage;
        }

        [Serializable]
        private class SignUpDependencies
        {
            [Header("Data to send")] public InputField email;
            public InputField password;
            public InputField confirmPassword;
            [Header("Render")] public GameObject currentPage;
            public GameObject nextPage;
        }

        [Header("Deps")] [SerializeField] private LogInDependencies logIn;
        [SerializeField] private SignUpDependencies signUp;

        [Header("Quest Render")] [SerializeField]
        private GameObject questPage;

        [Header("First buttons")] [SerializeField]
        private GameObject firstButtons;

        [Header("Views")] [SerializeField] private Categories.CategoriesView categoriesView;
        [SerializeField] private User.UserView userView;

        private IAccountWebService _accountWebService;

        protected override void Init()
        {
            _accountWebService = GetBean<IAccountWebService>();
        }

        public async void Quest()
        {
            Log("[Web Request] - Quest", LogType.Log);

            var req = new Request.Quest();
            var resp = await _accountWebService.Quest(req);

            Log("RenderQuest", LogType.Log);

            if (resp.IsSuccessful)
            {
                await categoriesView.GetAllCategories();
                await categoriesView.GetInterestingStories();
                logIn.currentPage.SetActive(false);
                signUp.currentPage.SetActive(false);
                firstButtons.SetActive(false);
                questPage.SetActive(true);
            }

            await userView.GetUser();
        }

        public async void SignIn()
        {
            var req = new Request.SignIn(logIn.email.Text, logIn.password.Text);
            Log($"[Web Request] - SignIn\nLogin: {req.userName}|Password: {req.password}",
                LogType.Log);

            var resp = await _accountWebService.SignIn(req);

            Log("Render SignIn", LogType.Log);
            var isSuccessful = resp.IsSuccessful;
            if (isSuccessful)
            {
                logIn.nextPage.SetActive(true);
                logIn.currentPage.SetActive(false);
            }

            string error = isSuccessful ? null : "Password of nickname is incorrect";

            logIn.password.UpdateState(error);
            logIn.email.UpdateState(error);

            if (isSuccessful)
            {
                await userView.GetUser();
                await categoriesView.GetAllCategories();
                await categoriesView.GetInterestingStories();
            }
        }

        public async void SignUp()
        {
            var passwordText = signUp.password.Text;
            var confirmPasswordText = signUp.confirmPassword.Text;
            var req = new Request.SignUp(signUp.email.Text, passwordText, confirmPasswordText);
            var arePasswordsDoNotMatch = passwordText != confirmPasswordText;
            var isPasswordHasSymbols = Regex.IsMatch(passwordText, PASWORD_PATTERN);
            var isPasswordNullOrEmpty = string.IsNullOrEmpty(passwordText);
            var isConfirmPasswordNullOrEmpty = string.IsNullOrEmpty(confirmPasswordText);
            if (isPasswordNullOrEmpty || isConfirmPasswordNullOrEmpty ||
                arePasswordsDoNotMatch || !isPasswordHasSymbols)
            {
                string error = arePasswordsDoNotMatch ? "Passwords do not match" :
                    !isPasswordHasSymbols ? "Password should have numbers and symbols" : null;
                signUp.password.UpdateState(isPasswordNullOrEmpty ? "Type password" : error);
                signUp.confirmPassword.UpdateState(isConfirmPasswordNullOrEmpty ? "Type password again" : error);

                Log($"{error}",
                    LogType.Error);
                return;
            }

            Log($"[Web Request] - SignUp\tLogin: {req.userName}|Password: {req.password}" +
                $"|ConfirmPassword: {req.confirmPassword}",
                LogType.Log);

            var resp = await _accountWebService.SignUp(req);
            await userView.GetUser();

            Log("Render SignUp", LogType.Log);

            var isSuccessful = resp.IsSuccessful;
            if (isSuccessful)
            {
                signUp.nextPage.SetActive(true);
                signUp.currentPage.SetActive(false);
            }

            var unknownError = isSuccessful ? null : "Unknown error";
            signUp.password.UpdateState(unknownError);
            signUp.email.UpdateState(unknownError);
            signUp.confirmPassword.UpdateState(unknownError);
        }
    }
}