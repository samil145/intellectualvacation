﻿using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Multiplayer;
using Networking.WebService.Multiplayer;

namespace Networking.View.Multiplayer
{
    public class MultiplayerHandler
    {
        private readonly NetworkManager _networkManager;
        private readonly IWebSocketMultiplayerService _webSocket;
        private readonly IRestMultiplayerService _rest;

        public MultiplayerHandler()
        {
            _networkManager = NetworkManager.Instance;
            _rest = _networkManager.Config.GetBean<IRestMultiplayerService>();
            _webSocket = _networkManager.Config.GetBean<IWebSocketMultiplayerService>();
        }

        public async Task<ConnectToLobbyResponse> ConnectSession(ConnectToLobbyRequest request)
        {
            var result = await _rest.ConnectToLobby(request);
            if (!result.IsSuccessful) return null;
            _webSocket.StartListen(result.Result.sessionId);
            return result.Result;
        }

        public async Task<WebResponse<GetQuizQuestionsResponse>> GetQuestions(GetQuizQuestionsRequest request)
        {
            var questions = await _rest.GetQuestions(request);
            return questions;
        }

        public async Task<WebResponse<QuestionAnswer>> Answer(AnswerQuizQuestionRequest request)
        {
            var resp = await _rest.AnswerQuestion(request);
            return resp;
        }

        public IWebSocketMultiplayerService WebSocket => _webSocket;
    }
}