﻿using UnityEngine;

namespace Networking.Multiplayer
{
    public class WinScreen : MonoBehaviour
    {
        [SerializeField] private TMPro.TMP_Text coinsChanged;
        [SerializeField] private TMPro.TMP_Text title;
        private ulong _coinsChanged = 0;
        private bool _isLocked = false;

        public void SetCoinsChange(ulong value)
        {
            if (!_isLocked)
            {
                _coinsChanged = value;
                coinsChanged.text = $"You gained: {_coinsChanged}";
            }
        }

        public void Refresh()
        {
            _coinsChanged = 0;
            _isLocked = false;
        }

        public void Lock()
        {
            _isLocked = true;
            title.text = "You won!";
        }
    }
}