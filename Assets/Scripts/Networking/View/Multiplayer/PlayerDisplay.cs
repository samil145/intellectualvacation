﻿using Networking.DTO.Multiplayer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Networking.View.Multiplayer
{
    public class PlayerDisplay : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text nickname;
        [SerializeField] private TMP_Text score;
        private Player _player;
        
        public void Init(Player player)
        {
            _player = player;
            image.sprite = player.Image;
            score.text = "Score: 0";
        }

        public void UpdateScore(int score)
        {
            this.score.text = $"Score: {score}";
        }

        public void Finish()
        {
            gameObject.SetActive(false);
        }
    }
}