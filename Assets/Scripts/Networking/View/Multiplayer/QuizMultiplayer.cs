﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IO;
using Networking.DTO.Multiplayer;
using Networking.DTO.User.Request;
using Networking.Multiplayer;
using Networking.View.User;
using Networking.WebService.User;
using TMPro;
using UnityEngine;
using UI;
using UI.Panel;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Networking.View.Multiplayer
{
    public class QuizMultiplayer : MonoBehaviour
    {
        class Fuck
        {
            public long AnswerId { get; set; }
            public QuizQuestion Question { get; set; }
        }

        [Header("Panels")] [FormerlySerializedAs("to")] [SerializeField]
        private GameObject secondScreen;

        [SerializeField] private PanelManager panelManager;
        private MultiplayerHandler _multiplayerHandler;
        private ConnectToLobbyResponse _connectToLobbyResult;
        private bool _isLoading;
        [SerializeField] private GameObject error;
        [SerializeField] private GameObject loading;
        [SerializeField] private TMPro.TMP_Text loadingText;

        [Header("Quiz stuff")] [SerializeField]
        private GameObject gamePlayPanel;

        [SerializeField] private Transform answerContent;
        [SerializeField] private Transform playersContent;
        [SerializeField] private GameObject answerButtonPrefab;
        [SerializeField] private PlayerDisplay playerDisplayPrefab;
        [SerializeField] private TMPro.TMP_Text questionText;
        private IDictionary<long, PlayerDisplay> _playerDisplays;
        [SerializeField] private UserView userView;
        [SerializeField] private WinScreen winScreen;
        private ConcurrentQueue<Func<Task>> _commands;
        [SerializeField] private Button nextButton;
        private Fuck _fuck;

        private void Start()
        {
            if (_multiplayerHandler == null)
            {
                _commands = new ConcurrentQueue<Func<Task>>();
                _multiplayerHandler = new MultiplayerHandler();
                _playerDisplays = new Dictionary<long, PlayerDisplay>();
                _fuck = new Fuck();
                nextButton.onClick.AddListener(Next);
            }
        }

        private void OnMatchEnded(MatchResult arg0)
        {
            _commands.Enqueue(async () =>
            {
                var currentUser = LocalStorage.Instance.Get<DTO.User.Response.User>("user");
                var userService = NetworkManager.Instance.Config.GetBean<IUserWebService>();
                var response = await userService.GetUser(new GetUser());
                var user = response.Result;
                var coinsChanged = user.stars - currentUser.stars;
                FinishGame();
                winScreen.SetCoinsChange(coinsChanged);
                winScreen.Lock();
                userView.RenderUser(user);
                _multiplayerHandler.WebSocket.Disconnect();
            });
        }

        private void OnProgressChanged(Progress arg0)
        {
            _commands.Enqueue(() =>
            {
                if (_playerDisplays.TryGetValue(arg0.userId, out var display))
                {
                    var currentUser = LocalStorage.Instance.Get<DTO.User.Response.User>("user");
                    display.UpdateScore(arg0.score);
                    if (arg0.hasFinished)
                    {
                        display.Finish();
                        if (currentUser.id == arg0.userId)
                        {
                            FinishGame();
                        }
                    }
                }

                return Task.CompletedTask;
            });
        }

        private void OnPlayerLobbyStateChanged(PlayerLobby arg0)
        {
            _commands.Enqueue(async () =>
            {
                if (arg0.isGameStarted)
                {
                    await StartGame();
                }
            });
        }

        private async Task StartGame()
        {
            var questions = await _multiplayerHandler.GetQuestions(
                new GetQuizQuestionsRequest() { sessionId = _connectToLobbyResult.sessionId });
            loadingText.text = "Getting data";
            if (questions.IsSuccessful)
            {
                _isLoading = false;
                loading.SetActive(false);
                gamePlayPanel.SetActive(true);
                secondScreen.SetActive(false);
                InitAllQuestions(questions.Result.questions);
            }
            else
            {
                error.SetActive(true);
            }
        }

        private void InitAllQuestions(List<QuizQuestion> questions)
        {
            int maxNumberOfAnswers = questions.Max(q => q.answers.Count);
            int currentNumberOfAnswers = answerContent.childCount;
            for (int i = 0; i < currentNumberOfAnswers; i++)
            {
                var child = answerContent.GetChild(i);
                child.gameObject.SetActive(false);
            }

            for (int i = currentNumberOfAnswers; i < maxNumberOfAnswers; i++)
            {
                var go = Instantiate(answerButtonPrefab, answerContent);
                go.SetActive(false);
            }

            QuizQuestion prev = null;
            foreach (var question in questions)
            {
                if (prev != null) prev.Next = question;
                prev = question;
            }

            InitQuestion(questions[0]);
        }

        private async void Update()
        {
            if (_commands.TryDequeue(out var command))
            {
                await command.Invoke();
            }
        }

        private void FinishGame()
        {
            gamePlayPanel.SetActive(false);
            secondScreen.SetActive(true);
            winScreen.gameObject.SetActive(true);
        }

        private void InitQuestion(QuizQuestion question)
        {
            questionText.text = question.text;
            var answers = question.answers;
            IEnumerable<(Toggle button, ToggleState state, TMP_Text buttonText, QuizAnswer answer)> shit = 
                Enumerable
                .Range(0, answers.Count).Select(i =>
                {
                    var buttonHolder = answerContent.GetChild(i);
                    buttonHolder.gameObject.SetActive(true);
                    var button = buttonHolder.GetComponentInChildren<Toggle>();
                    var buttonText = buttonHolder.GetComponentInChildren<TMP_Text>();
                    var answer = answers[i];
                    return (button, buttonHolder.GetComponent<ToggleState>(), buttonText, answer);
                }).ToList();
            foreach (var tuple in shit)
            {
                tuple.buttonText.text = tuple.answer.text;
                tuple.button.onValueChanged.RemoveAllListeners();
                tuple.state.SetState(false);
                tuple.button.onValueChanged.AddListener((var) =>
                {
                    _fuck.AnswerId = tuple.answer.id;
                    _fuck.Question = question;
                    tuple.state.SetState(true);
                    foreach (var valueTuple in shit.Where(x => x != tuple))
                    {
                        valueTuple.button.SetIsOnWithoutNotify(false);
                        valueTuple.state.SetState(false);
                    }
                });
            }
        }

        private async void Next()
        {
            var resp = await _multiplayerHandler.Answer(
                new AnswerQuizQuestionRequest()
                {
                    answerId = _fuck.AnswerId,
                    questionId = _fuck.Question.id,
                    sessionId = _connectToLobbyResult.sessionId
                }
            );
            if (resp.IsSuccessful)
            {
                var questionAnswer = resp.Result;
                if (!questionAnswer.wasLast)
                {
                    InitQuestion(_fuck.Question.Next);
                }
                else
                {
                    FinishGame();
                    winScreen.SetCoinsChange((ulong)questionAnswer.coinsChange);
                }
            }
            else
            {
                //TODO: Error;
            }
        }

        public async Task Begin(ConnectToLobbyRequest request)
        {
            error.SetActive(false);
            winScreen.gameObject.SetActive(false);
            if (IsMultiplayer)
            {
                //TODO: Magic number
                _isLoading = true;
                loading.SetActive(true);
                loadingText.text = "Searching session";
                _connectToLobbyResult = await _multiplayerHandler.ConnectSession(request);
                if (_connectToLobbyResult == null)
                {
                    _isLoading = false;
                    loading.SetActive(false);
                    error.SetActive(true);
                }
                else
                {
                    _multiplayerHandler.WebSocket.PlayerLobbyStateChanged += OnPlayerLobbyStateChanged;
                    _multiplayerHandler.WebSocket.ProgressChanged += OnProgressChanged;
                    _multiplayerHandler.WebSocket.MatchEnded += OnMatchEnded;
                    loadingText.text = "Waiting players";
                    _playerDisplays = new Dictionary<long, PlayerDisplay>();
                    var players = _connectToLobbyResult.players;
                    var playerHoldersCount = playersContent.childCount;
                    for (int i = playerHoldersCount; i < players.Count; i++)
                    {
                        var playerDisplay = Instantiate(playerDisplayPrefab, playersContent);
                        playerDisplay.gameObject.SetActive(false);
                    }

                    for (int i = 0; i < players.Count; i++)
                    {
                        var child = playersContent.GetChild(i);
                        child.gameObject.SetActive(true);
                        var playerDisplay = child.GetComponent<PlayerDisplay>();
                        var player = players[i];
                        _playerDisplays.Add(player.id, playerDisplay);
                        playerDisplay.Init(player);
                    }

                    for (int i = players.Count; i < playerHoldersCount; i++)
                    {
                        var child = playersContent.GetChild(i);
                        child.gameObject.SetActive(false);
                    }

                    if (_connectToLobbyResult.players.Count == _connectToLobbyResult.maxPlayerCount)
                    {
                        await StartGame();
                    }
                }
            }
            else
            {
                //TODO: Start Single Player
            }
        }

        public bool IsMultiplayer { get; set; }
    }
}