﻿using System.Collections.Generic;
using System.Linq;
using Networking.Logger;
using Networking.WebService.WebImage;
using Networking.WebService.WebImage.Impl;
using Networking.WebService;
using Networking.WebService.Account;
using Networking.WebService.Account.Impl;
using Networking.WebService.Categories;
using Networking.WebService.Categories.Impl;
using Networking.WebService.Multiplayer;
using Networking.WebService.Multiplayer.Impl;
using Networking.WebService.Story;
using Networking.WebService.Story.Impl;
using Networking.WebService.User;
using Networking.WebService.User.Impl;
using UnityEngine;
using ILogger = Networking.Logger.ILogger;

namespace Networking
{
    public class NetworkConfig
    {
        private readonly IDictionary<string, object> _singletoneBeans = new Dictionary<string, object>()
        {
            { Type2String<IAccountWebService>(), new ProdAccountWebService() },
            { Type2String<ICategoriesWebService>(), new ProdCategoriesWebService() },
            { Type2String<IUserWebService>(), new ProdUserWebService() },
            { Type2String<IStoryWebService>(), new ProdStoryWebService() },
            { Type2String<ILogger>(), new DebugLog() },
            { Type2String<IImageWebService>(), new ProdImageWebService() }, //GameObject.FindWithTag("Images").GetComponent<MockImageWebService>()
            { Type2String<IRestMultiplayerService>(), new ProdRestMultiplayerService() },
            { Type2String<IWebSocketMultiplayerService>(), new ProdWebSocketMultiplayerService() },
        };

        public void Init()
        {
            foreach (var bean in _singletoneBeans.Select(x => x.Value))
            {
                if (bean is IWebService webService)
                {
                    webService.Init();
                }
            }
        }

        public T GetBean<T>()
        {
            return (T)_singletoneBeans[Type2String<T>()];
        }

        private static string Type2String<T>()
        {
            return typeof(T).FullName;
        }
    }
}