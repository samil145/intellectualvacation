﻿using UnityEngine;

namespace Networking.Logger
{
    public interface ILogger
    {
        void Log(object message, LogType logType);
    }
}