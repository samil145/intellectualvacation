﻿using UnityEngine;

namespace Networking.Logger
{
    public class DebugLog : ILogger
    {
        void ILogger.Log(object message, LogType logType)
        {
            switch (logType)
            {
                case LogType.Error: Debug.LogError(message); break;
                case LogType.Assert: Debug.LogAssertion(message); break;
                case LogType.Warning: Debug.LogWarning(message); break;
                case LogType.Log: Debug.Log(message); break;
            }
        }
    }
}