﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using IO;
using Networking.DTO;
using Networking.DTO.Categories.Response;
using Networking.WebService.WebImage;
using Response = Networking.DTO.Categories.Response;

namespace Networking.WebService.Categories.Impl
{
    public class ProdCategoriesWebService : ICategoriesWebService
    {
        private const string CATEGORY = "Category/";

        private NetworkManager _manager;
        //private NetworkImageLoader _imageLoader;

        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
            //_imageLoader = _manager.NetworkImageLoader;
        }

        async Task<Response.Categories> ICategoriesWebService.Get()
        {
            var result = await _manager.SendAsync<Response.Categories>(CATEGORY, HttpMethod.Get, null);
            // foreach (var category in result)
            // {
            //     category.Image = await _imageLoader.GetImage(category.image);
            // }

            return result;
        }

        async Task<SetMyInterests> ICategoriesWebService.SetMyInterests(DTO.Categories.Request.SetMyInterests data)
        {
            SaveLoader.Instance.ListOfInterest = data.categories;

            return new SetMyInterests(new Empty(), HttpStatusCode.OK);
        }
    }
}