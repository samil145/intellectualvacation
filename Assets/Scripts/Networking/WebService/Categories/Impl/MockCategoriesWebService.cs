﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using IO;
using Networking.DTO;
using Networking.DTO.Categories;
using Networking.DTO.Categories.Request;
using Response = Networking.DTO.Categories.Response;
using Category = Networking.DTO.Categories.Category;

namespace Networking.WebService.Categories.Impl
{
    public class MockCategoriesWebService : ICategoriesWebService
    {
        void IWebService.Init()
        {
            
        }

        Task<Response.Categories> ICategoriesWebService.Get()
        {
            Response.Categories resp = new Response.Categories(new List<Category>()
            {
                new Category(1, "History"),
                new Category(2, "Astronomy"),
                new Category(3, "Literature"),
                new Category(4, "Mythology"),
                new Category(5, "Physics"),
                new Category(6, "Geography"),
            }, HttpStatusCode.OK);
            return Task.FromResult(resp);
        }

        Task<Response.SetMyInterests> ICategoriesWebService.SetMyInterests(SetMyInterests data)
        {
            SaveLoader.Instance.ListOfInterest = data.categories;
            
            return Task.FromResult(
                new Response.SetMyInterests(new Empty(), HttpStatusCode.OK));
        }
    }
}