﻿using System.Threading.Tasks;
using Networking.DTO;
using Response = Networking.DTO.Categories.Response;
using Request = Networking.DTO.Categories.Request;

namespace Networking.WebService.Categories
{
    public interface ICategoriesWebService : IWebService
    {
        Task<Response.Categories> Get();
        Task<Response.SetMyInterests> SetMyInterests(Request.SetMyInterests data);
    }
}