﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Networking.DTO.Multiplayer;
using UnityEngine;
using UnityEngine.Events;
using Utils.StompUtils;
using WebSocketSharp;

namespace Networking.WebService.Multiplayer.Impl
{
    public class ProdWebSocketMultiplayerService : IWebSocketMultiplayerService
    {
        private UnityEvent<PlayerLobby> _lobby;
        private UnityEvent<Progress> _progress;
        private UnityEvent<MatchResult> _result;

        private WebSocket _socket;
        private static readonly (string name, WebSocketMessageType value)[] _enumNames;

        static ProdWebSocketMultiplayerService()
        {
            _enumNames = Enum.GetNames(typeof(WebSocketMessageType))
                .Select(x => (x, Enum.Parse<WebSocketMessageType>(x)))
                .ToArray();
        }

        void IWebSocketMultiplayerService.StartListen(long sessionId)
        {
            (_socket as IDisposable)?.Dispose();
            _socket = new WebSocket(NetworkManager.WS_API_URL);
            var token = NetworkManager.Instance.GetAuthToken();
            //_socket.SetCookie(new WebSocketSharp.Net.Cookie("Authorization", "Bearer " + token.accessToken));git 
            _socket.OnMessage += SocketOnMessage;

            _socket.OnOpen += (sender, args) =>
            {
                var connect = new StompMessage(StompFrame.CONNECT);
                connect["accept-version"] = "1.2";
                connect["host"] = "";
                // first number Zero mean client not able to send Heartbeat, 
                //Second number mean Server will sending heartbeat to client instead
                connect["heart-beat"] = "0,10000";
                _socket.Send(StompMessageSerializer.Serialize(connect));
                
                var sub = new StompMessage(StompFrame.SUBSCRIBE);
                //unique Key per subscription
                sub["id"] = "sub-0"; 
                sub["destination"] = $"/topic/game/{sessionId}";
                _socket.Send(StompMessageSerializer.Serialize(sub));
            };
            _socket.Connect();

            _lobby = new UnityEvent<PlayerLobby>();
            _progress = new UnityEvent<Progress>();
            _result = new UnityEvent<MatchResult>();
        }

        private void SocketOnMessage(object sender, MessageEventArgs e)
        {
            // var json = System.Text.Encoding.UTF8.GetString(data);
            var json = e.Data.Split("\n\n")[1];
            foreach (var tuple in _enumNames)
            {
                json = json.Replace(tuple.name, ((int)tuple.value).ToString());
            }
            var raw = JsonUtility.FromJson<WebSocketMessage>(json);
            if (raw != null)
            {
                switch (raw.messageType)
                {
                    case WebSocketMessageType.Lobby:
                        _lobby.Invoke(JsonUtility.FromJson<PlayerLobby>(json));
                        break;
                    case WebSocketMessageType.Progress:
                        _progress.Invoke(JsonUtility.FromJson<Progress>(json));
                        break;
                    case WebSocketMessageType.MatchResult:
                        _result.Invoke(JsonUtility.FromJson<MatchResult>(json));
                        break;
                }
            }
        }

        public void Disconnect()
        {
            _socket.Close();
        }

        event UnityAction<PlayerLobby> IWebSocketMultiplayerService.PlayerLobbyStateChanged
        {
            add => _lobby.AddListener(value);
            remove => _lobby.RemoveListener(value);
        }

        event UnityAction<Progress> IWebSocketMultiplayerService.ProgressChanged
        {
            add => _progress.AddListener(value);
            remove => _progress.RemoveListener(value);
        }

        event UnityAction<MatchResult> IWebSocketMultiplayerService.MatchEnded
        {
            add => _result.AddListener(value);
            remove => _result.RemoveListener(value);
        }
    }
}