﻿using System.Net.Http;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Multiplayer;
using Networking.WebService.WebImage;

namespace Networking.WebService.Multiplayer.Impl
{
    public class ProdRestMultiplayerService : IRestMultiplayerService, IWebService
    {
        private const string MULTIPLAYER = "Multiplayer/";

        private NetworkManager _manager;
        private NetworkImageLoader _imageLoader;
        
        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
            _imageLoader = _manager.NetworkImageLoader;
        }
        
        async Task<WebResponse<ConnectToLobbyResponse>> IRestMultiplayerService.ConnectToLobby(ConnectToLobbyRequest request)
        {
            var result = await _manager.SendAsync<WebResponse<ConnectToLobbyResponse>>(
                MULTIPLAYER + "connect",
                HttpMethod.Post,
                request);
            foreach (var player in result.Result.players)
            {
                player.Image = await _imageLoader.GetImage(player.profilePhoto);
            }

            return result;
        }

        async Task<WebResponse<QuestionAnswer>> IRestMultiplayerService.AnswerQuestion(AnswerQuizQuestionRequest request)
        {
            var result = await _manager.SendAsync<WebResponse<QuestionAnswer>>(
                MULTIPLAYER + "answer-question",
                HttpMethod.Put,
                request);

            return result;
        }

        async Task<WebResponse<GetQuizQuestionsResponse>> IRestMultiplayerService.GetQuestions(GetQuizQuestionsRequest request)
        {
            var result = await _manager.SendAsync<WebResponse<GetQuizQuestionsResponse>>(
                MULTIPLAYER + $"session-questions/{request.sessionId}",
                HttpMethod.Get,
                null);

            return result;
        }

        async Task<WebResponse<ReconnectResponse>> IRestMultiplayerService.Reconnect()
        {
            var result = await _manager.SendAsync<WebResponse<ReconnectResponse>>(
                MULTIPLAYER + "reconnect",
                HttpMethod.Get,
                null);
            foreach (var player in result.Result.players)
            {
                player.Image = await _imageLoader.GetImage(player.profilePhoto);
            }

            return result;
        }
    }
}