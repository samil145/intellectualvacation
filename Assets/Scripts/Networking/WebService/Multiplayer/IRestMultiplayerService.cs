﻿using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Multiplayer;

namespace Networking.WebService.Multiplayer
{
    public interface IRestMultiplayerService
    {
        Task<WebResponse<ConnectToLobbyResponse>> ConnectToLobby(ConnectToLobbyRequest request);
        Task<WebResponse<QuestionAnswer>> AnswerQuestion(AnswerQuizQuestionRequest request);
        Task<WebResponse<GetQuizQuestionsResponse>> GetQuestions(GetQuizQuestionsRequest request);
        Task<WebResponse<ReconnectResponse>> Reconnect();
    }
}