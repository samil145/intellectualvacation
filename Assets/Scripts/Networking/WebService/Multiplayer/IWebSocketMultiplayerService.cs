﻿using System.Threading.Tasks;
using Networking.DTO.Multiplayer;
using UnityEngine.Events;

namespace Networking.WebService.Multiplayer
{
    public interface IWebSocketMultiplayerService
    {
        void StartListen(long sessionId);
        event UnityAction<PlayerLobby> PlayerLobbyStateChanged;
        event UnityAction<Progress> ProgressChanged;
        event UnityAction<MatchResult> MatchEnded;
        void Disconnect();
    }
}