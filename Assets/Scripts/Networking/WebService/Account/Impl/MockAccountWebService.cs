﻿using System.Net;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Account;
using Networking.DTO.Account.Response;

namespace Networking.WebService.Account.Impl
{
    public class MockAccountWebService : IAccountWebService
    {
        void IWebService.Init()
        {
            
        }

        Task<SignIn> IAccountWebService.SignIn(DTO.Account.Request.SignIn data)
        {
            Token token = new Token();
            token.accessToken = "asdasd.asda.asd";
            token.refreshToken = "asdasd.asda.asd";
            
            var response = new SignIn(token, HttpStatusCode.OK);
            
            return Task.FromResult(response);
        }

        Task<SignUp> IAccountWebService.SignUp(DTO.Account.Request.SignUp data)
        {
            Token token = new Token();
            token.accessToken = "asdasd.asda.asd";
            token.refreshToken = "asdasd.asda.asd";
            
            var response = new SignUp(token, HttpStatusCode.OK);
            
            return Task.FromResult(response);
        }

        Task<RefreshToken> IAccountWebService.RefreshToken(DTO.Account.Request.RefreshToken data)
        {
            Token token = new Token();
            token.accessToken = "asdasd.asda.asd";
            token.refreshToken = "asdasd.asda.asd";
            
            var response = new RefreshToken(token, HttpStatusCode.OK);
            
            return Task.FromResult(response);
        }

        Task<Quest> IAccountWebService.Quest(DTO.Account.Request.Quest data)
        {
            return Task.FromResult(new Quest(new Empty(), HttpStatusCode.OK));
        }
    }
}