﻿using System.Net.Http;
using System.Threading.Tasks;
using Networking.DTO.Account.Response;

namespace Networking.WebService.Account.Impl
{
    public class ProdAccountWebService : IAccountWebService
    {
        private const string AUTH = "Auth/";

        private NetworkManager _manager;

        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
        }

        async Task<SignIn> IAccountWebService.SignIn(DTO.Account.Request.SignIn data)
        {
            var result = await _manager.SendAsync<SignIn>(AUTH + "sign-in", HttpMethod.Post, data);
            if (result.IsSuccessful)
            {
                _manager.SetAuthToken(result.Result);
            }
            return result;
        }

        async Task<SignUp> IAccountWebService.SignUp(DTO.Account.Request.SignUp data)
        {
            var result = await _manager.SendAsync<SignUp>(AUTH + "sign-up", HttpMethod.Post, data);
            if (result.IsSuccessful)
            {
                _manager.SetAuthToken(result.Result);
            }
            return result;
        }

        Task<Quest> IAccountWebService.Quest(DTO.Account.Request.Quest data)
        {
            throw new System.NotImplementedException();
        }

        async Task<RefreshToken> IAccountWebService.RefreshToken(DTO.Account.Request.RefreshToken data)
        {
            var result = await _manager.SendAsync<RefreshToken>(AUTH + "refresh-token", HttpMethod.Post, data);
            _manager.SetAuthToken(result.Result);
            return result;
        }
    }
}