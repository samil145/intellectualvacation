using System.Threading.Tasks;
using Networking.DTO;
using Response = Networking.DTO.Account.Response;
using Request = Networking.DTO.Account.Request;

namespace Networking.WebService.Account
{
    public interface IAccountWebService : IWebService
    {
        Task<Response.SignIn> SignIn(Request.SignIn data);
        Task<Response.SignUp> SignUp(Request.SignUp data);
        Task<Response.Quest> Quest(Request.Quest data);
        Task<Response.RefreshToken> RefreshToken(Request.RefreshToken data);
    }
}