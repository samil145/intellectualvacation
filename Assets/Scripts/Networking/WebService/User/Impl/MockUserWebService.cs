﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Networking.DTO;
using Response = Networking.DTO.User.Response;
using Request = Networking.DTO.User.Request;

namespace Networking.WebService.User.Impl
{
    public class MockUserWebService : IUserWebService
    {
        private class User
        {
            public ulong id;
            public string email;
            public string password;
            public ulong stars;
        }

        private List<User> _users = new List<User>()
        {
            new User(){id = 1, email = "asd", password = "123", stars = 12}
        };

        void IWebService.Init()
        {
            
        }

        Task<WebResponse<Response.SetPhoto>> IUserWebService.SetPhoto(Request.SetPhoto data)
        {
            return Task.FromResult(new WebResponse<Response.SetPhoto>(new Response.SetPhoto(),HttpStatusCode.OK));
        }

        Task<WebResponse<Response.User>> IUserWebService.GetUser(Request.GetUser data)
        {
            var user = _users.FirstOrDefault();
            var result = new Response.User();
            if (user == null)
            {
                return Task.FromResult(new WebResponse<Response.User>(result,HttpStatusCode.NotFound));
            }
            result.email = user.email;
            result.stars = user.stars;
            return Task.FromResult(new WebResponse<Response.User>(result,HttpStatusCode.OK));
        }
    }
}