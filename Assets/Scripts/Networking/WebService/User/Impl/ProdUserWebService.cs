﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.User.Request;
using SetPhoto = Networking.DTO.User.Response.SetPhoto;

namespace Networking.WebService.User.Impl
{
    public class ProdUserWebService : IUserWebService
    {
        private NetworkManager _manager;

        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
        }

        async Task<WebResponse<SetPhoto>> IUserWebService.SetPhoto(DTO.User.Request.SetPhoto data)
        {
            var result = await _manager.SendAttachment<WebResponse<SetPhoto>>(
                "User/profile-photo",
                HttpMethod.Put,
                data.file,
                "file",
                $"photo.{data.fileExtension}"
            );
            return result;
        }

        async Task<WebResponse<DTO.User.Response.User>> IUserWebService.GetUser(GetUser data)
        {
            var result = await _manager.SendAsync<WebResponse<DTO.User.Response.User>>(
                "User/me",
                HttpMethod.Get,
                null
            );
            return result;
        }
    }
}