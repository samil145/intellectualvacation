﻿using System.Net;
using System.Threading.Tasks;
using Networking.DTO;
using Request = Networking.DTO.User.Request;
using Response = Networking.DTO.User.Response;

namespace Networking.WebService.User
{
    public interface IUserWebService : IWebService
    {
        Task<WebResponse<Response.SetPhoto>> SetPhoto(Request.SetPhoto data);
        Task<WebResponse<Response.User>> GetUser(Request.GetUser data);
    }
}