﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Story;
using Networking.DTO.Story.Response;
using Networking.WebService.WebImage;

namespace Networking.WebService.Story.Impl
{
    public class ProdStoryWebService : IStoryWebService
    {
        private const string STORY = "Story/";
        private const string LEVEL = "Level/";

        private NetworkManager _manager;
        private NetworkImageLoader _imageLoader;

        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
            _imageLoader = _manager.NetworkImageLoader;
        }

        async Task<StoriesForYou> IStoryWebService.GetStoriesForYou(DTO.Story.Request.StoriesForYou data)
        {
            string query = "?ids=" + string.Join(',', data.idsOfInterests);
            var result = await _manager.SendAsync<StoriesForYou>(STORY + "for-you" + query, HttpMethod.Get, null);
            // foreach (var story in result)
            // {
            //     story.Sprite = await _imageLoader.GetImage(story.image);
            // }

            return result;
        }

        async Task<StoriesAll> IStoryWebService.GetStoriesByCategoryId(DTO.Story.Request.StoriesAll data)
        {
            var result = await _manager.SendAsync<StoriesAll>(
                STORY + $"{data.id}/category",
                HttpMethod.Get,
                null);
            // foreach (var story in result)
            // {
            //     story.Sprite = await _imageLoader.GetImage(story.image);
            // }

            return result;
        }

        async Task<Levels> IStoryWebService.GetLevels(DTO.Story.Request.Levels data)
        {
            var result = await _manager.SendAsync<Levels>(
                STORY + $"{data.id}/levels",
                HttpMethod.Get,
                null);
            return result;
        }

        async Task<BuyLevel> IStoryWebService.BuyLevel(DTO.Story.Request.BuyLevel data)
        {
            var result = await _manager.SendAsync<BuyLevel>(
                LEVEL + $"{data.id}/purchase",
                HttpMethod.Put,
                null);
            return result;
        }

        async Task<WebResponse<List<LevelItem>>> IStoryWebService.GetLevel(ulong id)
        {
            var result = await _manager.SendAsync<WebResponse<List<LevelItem>>>(
                LEVEL + $"{id}/level-items",
                HttpMethod.Get,
                null);
            foreach (var levelItem in result.Result)
            {
                levelItem.CharacterImage = await _imageLoader.GetImage(levelItem.characterImage);
                levelItem.LocationImage = await _imageLoader.GetImage(levelItem.locationImage);
            }

            return result;
        }
    }
}