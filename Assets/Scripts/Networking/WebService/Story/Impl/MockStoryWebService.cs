﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Story;
using Networking.DTO.Story.Response;
using Networking.WebService.WebImage;

namespace Networking.WebService.Story.Impl
{
    public class MockStoryWebService : IStoryWebService
    {
        private List<DTO.Story.Story> _stories = new List<DTO.Story.Story>()
        {
            new DTO.Story.Story() { id = 1, categoryId = 1, name = "1 1", image = new Image()
            {
                id = 1
            }, description = "Cool story"},
            new DTO.Story.Story() { id = 2, categoryId = 1, name = "2 1", image = new Image()
            {
                id = 2
            }},
            new DTO.Story.Story() { id = 3, categoryId = 2, name = "3 2", image = new Image()
            {
                id = 3
            }},
            new DTO.Story.Story() { id = 4, categoryId = 2, name = "4 2", image = new Image()
            {
                id = 4
            }},

            new DTO.Story.Story() { id = 5, categoryId = 3, name = "5 3", image = new Image()
            {
                id = 5
            }},

            new DTO.Story.Story() { id = 6, categoryId = 4, name = "6 4", image = new Image()
            {
                id = 6
            }},
            new DTO.Story.Story() { id = 7, categoryId = 4, name = "7 4", image = new Image()
            {
                id = 7
            }},
            new DTO.Story.Story() { id = 8, categoryId = 4, name = "8 4", image = new Image()
            {
                id = 8
            }},

            new DTO.Story.Story() { id = 9, categoryId = 5, name = "9 5", image = new Image()
            {
                id = 9
            }},
            new DTO.Story.Story() { id = 10, categoryId = 5, name = "10 5",  image = new Image()
            {
                id = 10
            }},
        };

        private List<DTO.Story.Level> _levels = new List<DTO.Story.Level>()
        {
            new DTO.Story.Level()
            {
                id = 1, StoryId = 1, isPurchased = true, cost = 10,
                items = new List<LevelItem>()
                {
                    new LevelItem()
                    {
                        characterImage = new Image() { id = 11 },
                        locationImage = new Image() { id = 12 },
                        text = "sdasdsaasdasddasasd",
                    },
                    new LevelItem()
                    {
                        characterImage = null,
                        locationImage = new Image() { id = 12 },
                        text = "=========",
                    },
                    new LevelItem()
                    {
                        characterImage = null,
                        locationImage = new Image() { id = 12 },
                        text = "Vopros",
                        storyAnswers = new List<Answer>()
                        {
                            new Answer() { isCorrect = false, text = "A" },
                            new Answer() { isCorrect = false, text = "B" },
                            new Answer() { isCorrect = true, text = "C" },
                            new Answer() { isCorrect = false, text = "D" },
                        }
                    },             
                    new LevelItem()
                    {
                        characterImage = new Image() { id = 11 },
                        locationImage = new Image() { id = 12 },
                        text = "Lox",
                    },
                }
            },
            new DTO.Story.Level() { id = 2, StoryId = 1, isPurchased = true, cost = 20 },
            new DTO.Story.Level() { id = 3, StoryId = 1, isPurchased = false, cost = 10 },
            new DTO.Story.Level() { id = 4, StoryId = 1, isPurchased = false, cost = 20 },

            new DTO.Story.Level() { id = 5, StoryId = 2 },
            new DTO.Story.Level() { id = 6, StoryId = 2 },
            new DTO.Story.Level() { id = 7, StoryId = 2 },
            new DTO.Story.Level() { id = 8, StoryId = 2 },

            new DTO.Story.Level() { id = 9, StoryId = 3 },
            new DTO.Story.Level() { id = 10, StoryId = 3 },
            new DTO.Story.Level() { id = 11, StoryId = 3 },
            new DTO.Story.Level() { id = 12, StoryId = 3 },

            new DTO.Story.Level() { id = 13, StoryId = 3 },
            new DTO.Story.Level() { id = 14, StoryId = 3 },
            new DTO.Story.Level() { id = 15, StoryId = 3 },
            new DTO.Story.Level() { id = 16, StoryId = 3 },

            new DTO.Story.Level() { id = 17, StoryId = 4 },
            new DTO.Story.Level() { id = 18, StoryId = 4 },
            new DTO.Story.Level() { id = 19, StoryId = 4 },
            new DTO.Story.Level() { id = 20, StoryId = 4 },

            new DTO.Story.Level() { id = 21, StoryId = 5 },
            new DTO.Story.Level() { id = 22, StoryId = 5 },
            new DTO.Story.Level() { id = 23, StoryId = 5 },
            new DTO.Story.Level() { id = 24, StoryId = 5 },
        };

        private NetworkImageLoader _imageLoader;

        void IWebService.Init()
        {
            _imageLoader = NetworkManager.Instance.NetworkImageLoader;
        }

        async Task<StoriesForYou> IStoryWebService.GetStoriesForYou(DTO.Story.Request.StoriesForYou data)
        {
            var stories = _stories
                .Where(x => data.idsOfInterests.Contains(x.categoryId))
                .ToList();
            foreach (var story in stories)
            {
                story.Sprite = await _imageLoader.GetImage(story.image);
            }

            var resp = new StoriesForYou(stories, HttpStatusCode.OK);
            return resp;
        }

        async Task<StoriesAll> IStoryWebService.GetStoriesByCategoryId(DTO.Story.Request.StoriesAll data)
        {
            var stories = _stories.Where(x => x.categoryId == data.id).ToList();
            foreach (var story in stories)
            {
                story.Sprite = await _imageLoader.GetImage(story.image);
            }

            var resp = new StoriesAll(stories,
                HttpStatusCode.OK);
            return resp;
        }

        Task<Levels> IStoryWebService.GetLevels(DTO.Story.Request.Levels data)
        {
            var resp = new Levels(_levels.Where(x => x.StoryId == data.id).ToList(), HttpStatusCode.OK);
            return Task.FromResult(resp);
        }

        Task<BuyLevel> IStoryWebService.BuyLevel(DTO.Story.Request.BuyLevel data)
        {
            var res = _levels.SingleOrDefault(x => x.id == data.id);
            if (res != null)
            {
                res.isPurchased = true;
            }
            return Task.FromResult(new BuyLevel(res, HttpStatusCode.OK));
        }

        async Task<WebResponse<List<LevelItem>>> IStoryWebService.GetLevel(ulong id)
        {
            var level = _levels.Single(x => x.id == id);
            foreach (var levelItem in level.items)
            {
                if (levelItem.characterImage != null)
                {
                    levelItem.CharacterImage = await _imageLoader.GetImage(levelItem.characterImage);
                }

                if (levelItem.locationImage != null)
                {
                    levelItem.LocationImage = await _imageLoader.GetImage(levelItem.locationImage);
                }
            }

            return new WebResponse<List<LevelItem>>(level.items, HttpStatusCode.OK);
        }
    }
}