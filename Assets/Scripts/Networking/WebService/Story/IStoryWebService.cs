﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Story;
using UnityEditor.PackageManager.Requests;
using Response = Networking.DTO.Story.Response;
using Request = Networking.DTO.Story.Request;

namespace Networking.WebService.Story
{
    public interface IStoryWebService : IWebService
    {
        Task<Response.StoriesForYou> GetStoriesForYou(Request.StoriesForYou data);
        Task<Response.StoriesAll> GetStoriesByCategoryId(Request.StoriesAll data);
        Task<Response.Levels> GetLevels(Request.Levels data);
        Task<Response.BuyLevel> BuyLevel(Request.BuyLevel data);
        Task<WebResponse<List<LevelItem>>> GetLevel(ulong id);
    }
}