﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.WebService;
using UnityEngine;

namespace Networking.WebService.WebImage.Impl
{
    public class MockImageWebService : MonoBehaviour, IImageWebService
    {
        [Serializable]
        private class Pair
        {
            public ulong id;
            public Texture2D spriteAsset;
            public string path;
        }

        [SerializeField] private List<Pair> images;

        void IWebService.Init()
        {
        }

        async Task<WebResponse<ImageBytes>> IImageWebService.GetImage(DTO.Image image)
        {
            var result = images
                .SingleOrDefault(x => x.id == image.id);
            
            if (result != null)
                return new WebResponse<ImageBytes>(
                    new ImageBytes()
                    {
                        bytes = string.Concat(await File.ReadAllBytesAsync(result.path)),
                        format = result.path.Split('.').Last()
                    },
                    HttpStatusCode.OK
                );
            return null;
        }
    }
}