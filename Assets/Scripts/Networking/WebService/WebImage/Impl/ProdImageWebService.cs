﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.WebService;

namespace Networking.WebService.WebImage.Impl
{
    public class ProdImageWebService : IImageWebService
    {
        private NetworkManager _manager;

        void IWebService.Init()
        {
            _manager = NetworkManager.Instance;
        }

        public async Task<WebResponse<ImageBytes>> GetImage(DTO.Image id)
        {
            var res = await _manager.SendAsync<WebResponse<ImageBytes>>(
                $"Image/bytes?imageId={id.id}",
                HttpMethod.Get,
                null
            );
            return res;
        }
    }
}