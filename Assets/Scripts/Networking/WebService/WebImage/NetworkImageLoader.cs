﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Networking.DTO;
using UnityEngine;

namespace Networking.WebService.WebImage
{
    public class NetworkImageLoader
    {
        private readonly IDictionary<ulong, Sprite> _sprites;
        private readonly IImageWebService _imageWebService;
        private readonly ConcurrentDictionary<Image, Task<WebResponse<ImageBytes>>> _tasks;

        public NetworkImageLoader(IImageWebService imageWebService)
        {
            _imageWebService = imageWebService;
            _sprites = new Dictionary<ulong, Sprite>();
            _tasks = new ConcurrentDictionary<Image, Task<WebResponse<ImageBytes>>>();
        }

        public async Task<Sprite> GetImage(DTO.Image image)
        {
            if (image == null || image.id == 0)
            {
                return null;
            }

            var imageID = image.id;

            Debug.Log($"Fetching image with id {imageID}");
            var hasValue = _sprites.TryGetValue(imageID, out var sprite);
            //TODO: Too much calls
            if (!hasValue)
            {
                var raw = await _tasks.GetOrAdd(image,
                    async arg => await _imageWebService.GetImage(arg)
                );
                if (raw?.IsSuccessful ?? false)
                {
                    TextureFormat format = GetTextureFormat(raw.Result.format);
                    Texture2D tex = new Texture2D(2, 2, format, true);
                    var rawBytes = Convert.FromBase64String(raw.Result.bytes);
                    if (!tex.LoadImage(rawBytes, true))
                    {
                        Debug.LogError($"[{nameof(NetworkImageLoader)}] - Image was unable to load!");
                    }

                    sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one / 2f);
                    _sprites.TryAdd(imageID, sprite);
                }
            }

            Debug.Log($"Fetched image with id {imageID}");

            return sprite;
        }

        private TextureFormat GetTextureFormat(string format)
        {
            return format switch
            {
                "png" => TextureFormat.ARGB32,
                "jpg" => TextureFormat.RGB24
            };
        }
    }
}