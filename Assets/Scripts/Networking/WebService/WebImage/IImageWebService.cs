﻿using System.Threading.Tasks;
using Networking.DTO;
using Networking.WebService;

namespace Networking.WebService.WebImage
{
    public interface IImageWebService : IWebService
    {
        Task<WebResponse<ImageBytes>> GetImage(DTO.Image id);
    }
}