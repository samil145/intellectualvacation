﻿using System;

namespace Networking
{
    [Serializable]
    public class Token
    {
        public string accessToken;
        public string refreshToken;

        public override string ToString()
        {
            return $"{nameof(accessToken)}: {accessToken}, {nameof(refreshToken)}: {refreshToken}";
        }
    }
}