﻿using System;
using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO
{
    public class WebResponse<TResponse>
    {
        public TResponse result;
        public HttpStatusCode status;
        public bool hasErrors;

        public WebResponse()
        {
            status = HttpStatusCode.BadRequest;
            hasErrors = true;
        }
        
        public WebResponse([NotNull] TResponse response, HttpStatusCode status)
        {
            result = response ?? throw new ArgumentNullException(nameof(response));
            this.status = status;
            hasErrors = (int)this.status / 100 >= 4;
        }

        public TResponse Result => result;

        public bool IsSuccessful => !hasErrors;

        public HttpStatusCode Status => status;
    }
}