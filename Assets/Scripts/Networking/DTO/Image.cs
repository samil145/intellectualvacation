﻿using System;

namespace Networking.DTO
{
    [Serializable]
    public class Image
    {
        public ulong id;

        public override bool Equals(object obj)
        {
            var other = obj as Image;
            if (other == null) return false;
            return id.Equals(other.id);
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}