﻿namespace Networking.DTO.Account.Request
{
    public class SignUp
    {
        public string userName;
        public string password;
        public string confirmPassword;
        
        public SignUp(string userName, string password, string confirmPassword)
        {
            this.userName = userName;
            this.password = password;
            this.confirmPassword = confirmPassword;
        }
    }
}