﻿namespace Networking.DTO.Account.Request
{
    public class SignIn
    {
        public string userName;
        public string password;

        public SignIn(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }
    }
}