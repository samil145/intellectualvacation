﻿namespace Networking.DTO.Account.Request
{
    public class RefreshToken
    {
        public string refreshToken;

        public RefreshToken(string refreshToken)
        {
            this.refreshToken = refreshToken;
        }
    }
}