﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Account.Response
{
    public class Quest : WebResponse<Empty>
    {
        public Quest([NotNull] Empty response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}