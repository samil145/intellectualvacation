﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Account.Response
{
    public class SignIn : WebResponse<Token>
    {
        public SignIn()
        {
            
        }
        
        public SignIn([NotNull] Token response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}