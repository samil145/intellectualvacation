﻿namespace Networking.DTO.Account.Response
{
    public class TokenError : WebResponse<TokenError.Error>
    {
        public class Error
        {
            public string tokenError;
        }
    }
}