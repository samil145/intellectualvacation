﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Account.Response
{
    public class RefreshToken : WebResponse<Token>
    {
        public RefreshToken()
        {
            
        }
        
        public RefreshToken([NotNull] Token response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}