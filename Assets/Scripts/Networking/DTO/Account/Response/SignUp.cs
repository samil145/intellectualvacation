﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Account.Response
{
    public class SignUp : WebResponse<Token>
    {
        public SignUp()
        {
            
        }
        
        public SignUp([NotNull] Token response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}