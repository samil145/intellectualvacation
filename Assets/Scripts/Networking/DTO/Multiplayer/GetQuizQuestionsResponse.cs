﻿using System;
using System.Collections.Generic;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class GetQuizQuestionsResponse
    {
        public List<QuizQuestion> questions;
    }
}