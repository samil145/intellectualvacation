﻿using System;
using System.Collections.Generic;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class MatchResult : WebSocketMessage
    {
        public List<long> winnerUserIDs;
    }
}