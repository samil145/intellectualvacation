﻿using System;
using UnityEngine;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class Player {
        public long id;
        public string nickname;
        public Image profilePhoto;
        
        private Sprite _image;
        
        public Sprite Image
        {
            get => _image;
            set => _image = value;
        }
    }
}