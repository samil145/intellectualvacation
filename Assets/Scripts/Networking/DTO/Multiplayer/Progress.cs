﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class Progress : WebSocketMessage
    {
        public long userId;
        public int score;
        public bool hasFinished;
    }
}