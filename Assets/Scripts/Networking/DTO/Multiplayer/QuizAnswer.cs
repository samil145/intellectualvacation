﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class QuizAnswer
    {
        public long id;
        public string text;
    }
}