﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class AnswerQuizQuestionRequest
    {
        public long sessionId;
        public long questionId;
        public long answerId;
    }
}