﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class WebSocketMessage
    {
        public WebSocketMessageType messageType;
    }
}