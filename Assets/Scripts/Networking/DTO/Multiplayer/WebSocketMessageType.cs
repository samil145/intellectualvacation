﻿namespace Networking.DTO.Multiplayer
{
    public enum WebSocketMessageType
    {
        Lobby,
        Progress,
        MatchResult,
    }
}