﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class ConnectToLobbyRequest
    {
        public ulong categoryId;
    }
}