﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class PlayerLobby : WebSocketMessage
    {
        // sends special player entity instead of ture user entity as no need to show coins of user
        public Player user;
        public PlayerLobbyState state;
        public bool isGameStarted;
    }
}