﻿namespace Networking.DTO.Multiplayer
{
    public enum MatchOverState
    {
        WON,
        LOST
    }
}