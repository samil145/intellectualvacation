﻿using System;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class QuestionAnswer
    {
        public bool isCorrect;
        public bool wasLast;
        public int coinsChange;
    }
}