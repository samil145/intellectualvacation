﻿using System;
using System.Collections.Generic;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class ReconnectResponse
    {
        public long sessionId;
        public List<Player> players;
        public List<QuizQuestion> questions;
    }
}