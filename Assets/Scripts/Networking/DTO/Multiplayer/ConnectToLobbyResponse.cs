﻿using System;
using System.Collections.Generic;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class ConnectToLobbyResponse
    {
        public long sessionId;
        public int maxPlayerCount;
        public List<Player> players;
    }
}