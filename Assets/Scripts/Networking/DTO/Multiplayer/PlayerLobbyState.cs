﻿namespace Networking.DTO.Multiplayer
{
    public enum PlayerLobbyState {
        JOINED,
        READY,
        NOT_READY,
        QUIT
    }
}