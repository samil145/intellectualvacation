﻿using System;
using System.Collections.Generic;

namespace Networking.DTO.Multiplayer
{
    [Serializable]
    public class QuizQuestion
    {
        public long id;
        public string text;
        public List<QuizAnswer> answers;
        public QuizQuestion Next { get; set; }
    }
}