﻿using System.Collections.Generic;
using System.Linq;

namespace Networking.DTO.Categories.Request
{
    public class SetMyInterests
    {
        public List<ulong> categories;

        public override string ToString()
        {
            return $"Categories: {string.Join(", ", categories)}";
        }
    }
}