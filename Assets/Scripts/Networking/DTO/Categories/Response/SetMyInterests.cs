﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Categories.Response
{
    public class SetMyInterests : WebResponse<Empty>
    {
        public SetMyInterests([NotNull] Empty response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}