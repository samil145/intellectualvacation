﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using JetBrains.Annotations;

namespace Networking.DTO.Categories.Response
{
    public class Categories : WebResponse<List<Category>>, IEnumerable<Category>
    {
        public Categories()
        {
            
        }
        
        public Categories([NotNull] List<Category> response, HttpStatusCode status) : base(response, status)
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<Category> GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder(Result.Count);
            foreach (var category in Result)
            {
                stringBuilder.Append($"{{{category}}},");
            }
            return $"{nameof(Result)}: {Result}";
        }
    }
}