﻿using System;
using System.Collections.Generic;
using UI;
using UnityEngine;
using Utils;

namespace Networking.DTO.Categories
{
    [Serializable]
    public class Category : IId, INetworkImage
    {
        public ulong id;
        public string name;
        public Image image;
        
        private Sprite _image;
        
        public Category()
        {
        }

        public Category(ulong id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public override string ToString()
        {
            return $"{nameof(id)}: {id}, {nameof(name)}: {name}";
        }

        public ulong ID => id;

        public Sprite Image
        {
            get => _image;
            set => _image = value;
        }

        public Image NetworkImage => image;
    }
}