﻿using System;

namespace Networking.DTO
{
    [Serializable]
    public class ImageBytes
    {
        public string bytes;
        public string format;
    }
}