﻿namespace Networking.DTO.User.Request
{
    public class SetPhoto
    {
        public byte[] file;
        public string fileExtension;
    }
}