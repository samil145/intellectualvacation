﻿using System;

namespace Networking.DTO.User.Response
{
    [Serializable]
    public class User
    {
        public long id;
        public string email;
        public ulong stars;
        public ulong imageId;
        
        public override string ToString()
        {
            return $"{nameof(id)}: {id}, {nameof(email)}: {email}, {nameof(stars)}: {stars}, {nameof(imageId)}: {imageId}";
        }
    }
}