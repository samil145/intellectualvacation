﻿using System.Collections.Generic;

namespace Networking.DTO.Story.Request
{
    public class StoriesForYou
    {
        public List<ulong> idsOfInterests;
    }
}