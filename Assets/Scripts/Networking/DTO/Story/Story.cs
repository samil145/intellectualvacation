﻿using System;
using UI;
using UnityEngine;
using Utils;

namespace Networking.DTO.Story
{
    [Serializable]
    public class Story : IId, INetworkImage
    {
        public ulong id;
        public string name;
        public string description;
        public ulong categoryId;
        public Image image;
        public Image levelsBgImage;
        private Sprite _sprite;

        public override string ToString()
        {
            return $"{nameof(id)}: {id}, {nameof(name)}: {name}, {nameof(description)}: {description}, {nameof(categoryId)}: {categoryId}";
        }

        public ulong ID => id;

        public Sprite Sprite
        {
            get => _sprite;
            set => _sprite = value;
        }

        public Image NetworkImage => image;
    }
}