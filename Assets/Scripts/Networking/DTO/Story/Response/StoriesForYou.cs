﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Story.Response
{
    public class StoriesForYou : WebResponse<List<Story>>, IEnumerable<Story>
    {
        public StoriesForYou()
        {
            
        }
        
        public StoriesForYou([NotNull] List<Story> response, HttpStatusCode status) : base(response, status)
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        IEnumerator<Story> IEnumerable<Story>.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        public override string ToString()
        {
            return $"{nameof(Result)}: {string.Join(", ", Result)}";
        }
    }
}