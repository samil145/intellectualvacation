﻿using System.Collections.Generic;
using System.Collections;
using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Story.Response
{
    public class StoriesAll : WebResponse<List<Story>>, IEnumerable<Story>
    {
        public StoriesAll()
        {
            
        }
        
        public StoriesAll([NotNull] List<Story> response, HttpStatusCode status) : base(response, status)
        {
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        IEnumerator<Story> IEnumerable<Story>.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        public override string ToString()
        {
            return $"{nameof(Result)}: {string.Join(", ", Result)}";
        }
    }
}