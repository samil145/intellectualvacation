﻿using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Story.Response
{
    public class BuyLevel : WebResponse<Level>
    {
        public BuyLevel()
        {
            
        }
        
        public BuyLevel([NotNull] Level response, HttpStatusCode status) : base(response, status)
        {
        }
    }
}