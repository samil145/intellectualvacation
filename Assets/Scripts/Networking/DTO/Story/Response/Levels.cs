﻿using System.Collections.Generic;
using System.Collections;
using System.Net;
using JetBrains.Annotations;

namespace Networking.DTO.Story.Response
{
    public class Levels: WebResponse<List<Level>>, IEnumerable<Level>
    {
        public Levels()
        {
            
        }
        
        public Levels([NotNull] List<Level> response, HttpStatusCode status) : base(response, status)
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        IEnumerator<Level> IEnumerable<Level>.GetEnumerator()
        {
            return Result.GetEnumerator();
        }

        public override string ToString()
        {
            return $"{nameof(Result)}: {string.Join(", ", Result)}";
        }
    }
}