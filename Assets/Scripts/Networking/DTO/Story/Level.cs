﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Utils;

namespace Networking.DTO.Story
{
    [Serializable]
    public class Level : IId
    {
        public ulong id;
        public string name;
        public bool isPurchased;
        public ulong cost;
        public List<LevelItem> items;

        public override string ToString()
        {
            return
                $"{nameof(id)}: {id}, {nameof(name)}: {name}, {nameof(isPurchased)}: {isPurchased}, {nameof(cost)}: {cost}, {nameof(items)}: {items}";
        }

        public ulong ID => id;

        public ulong StoryId { get; set; }
        
        public int Order { get; set; }
    }
}