﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Utils;

namespace Networking.DTO.Story
{
    [Serializable]
    public class LevelItem
    {
        public Image locationImage;  
        public Image characterImage;
        public string text;
        public List<Answer> storyAnswers;
        
        private Sprite _characterImage;
        private Sprite _locationImage;

        public Sprite CharacterImage
        {
            get => _characterImage;
            set => _characterImage = value;
        }

        public Sprite LocationImage
        {
            get => _locationImage;
            set => _locationImage = value;
        }
    }
}