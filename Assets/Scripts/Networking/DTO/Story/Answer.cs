﻿using System;
using Utils;

namespace Networking.DTO.Story
{
    [Serializable]
    public class Answer
    {
        public bool isCorrect;
        public string text;
    }
}