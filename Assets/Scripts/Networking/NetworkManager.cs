using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using UnityEngine;
using System.Threading.Tasks;
using Networking.DTO;
using Networking.DTO.Account.Response;
using ILogger = Networking.Logger.ILogger;
using Networking.WebService;
using Networking.WebService.WebImage;
using WebSocketSharp;

namespace Networking
{
    public class NetworkManager : MonoBehaviour
    {
        public const string SERVER_URL = "localhost:8080/";
        public const string API_API_URL = SERVER_URL + "api/v1/";
        public const string REST_API_URL = "http://" + API_API_URL;
        public const string WS_API_URL = "ws://" + SERVER_URL + "ws";
        
        private static NetworkManager _instance;
        private HttpClient _client;
        private NetworkConfig _config;
        private NetworkImageLoader _networkImageLoader;
        private Token _token;
        private ILogger _logger;

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this;

            _config = new NetworkConfig();
            _networkImageLoader = new NetworkImageLoader(_config.GetBean<IImageWebService>());
            _config.Init();

            _logger = _config.GetBean<ILogger>();

            _client = new HttpClient(new HttpClientHandler(), true);
            _client.BaseAddress = new Uri(REST_API_URL);
            var accept = _client.DefaultRequestHeaders.Accept;
            accept.Add(new MediaTypeWithQualityHeaderValue(
                    "application/json"
                )
            );
            accept.Add(new MediaTypeWithQualityHeaderValue(
                    "multipart/form-data"
                )
            );

            DontDestroyOnLoad(gameObject);
        }

        public async Task<TResponse> SendAsync<TResponse>(string url, HttpMethod httpMethod, object obj)
            where TResponse : new()
        {
            HttpContent httpContent = null;
            if (obj != null)
            {
                string body = JsonUtility.ToJson(obj);
                httpContent = new StringContent(body, Encoding.UTF8, "application/json");
            }

            return await Send<TResponse>(url, httpMethod, httpContent);
        }

        public async Task<TResponse> SendAttachment<TResponse>(string url, HttpMethod httpMethod, byte[] data,
            string name, string fileName) where TResponse : new()
        {
            HttpContent content = new ByteArrayContent(data);
            
            var form = new MultipartFormDataContent();
            form.Add(content, name,fileName);

            return await Send<TResponse>(url, httpMethod, form);
        }


        private async Task<TResponse> Send<TResponse>(string url, HttpMethod httpMethod, HttpContent content)
            where TResponse : new()
        {
            TResponse result = new TResponse();
            HttpStatusCode statusCode;
            bool isSuccessful = false;
            string message = "null";

            using (HttpRequestMessage requestMessage = new HttpRequestMessage(httpMethod, url))
            {
                if (content != null)
                {
                    requestMessage.Content = content;
                }

                var webResponse = await _client.SendAsync(requestMessage);

                statusCode = webResponse.StatusCode;
                //TODO: Should be #if DEBUG be added?
                message = await webResponse.Content.ReadAsStringAsync();

                if (webResponse.IsSuccessStatusCode)
                {
                    result = JsonUtility.FromJson<TResponse>(message);
                    isSuccessful = true;
                }
                //TODO: Refresh token testing required
                else if (webResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    var tokenError = JsonUtility.FromJson<TokenError>(message);
                    _logger.Log($"[{nameof(NetworkManager)}] - Unauthorized\n{tokenError.Result.tokenError}",
                        LogType.Error);

                    var newToken = await SendAsync<RefreshToken>("Auth/refresh-token", HttpMethod.Post,
                        new DTO.Account.Request.RefreshToken(_token.refreshToken));
                    if (newToken != null)
                    {
                        SetAuthToken(newToken.Result);
                        return await Send<TResponse>(url, httpMethod, content);
                    }
                    else
                    {
                        _logger.Log($"[{nameof(NetworkManager)}] - Pizdes", LogType.Error);
                    }
                }
            }

            if (isSuccessful)
            {
                _logger.Log($"[NetworkManager] - Successful Respond\n{message}", LogType.Log);
            }
            else
            {
                _logger.Log($"[NetworkManager] - Unsuccessful Respond\tStatusCode: {statusCode}" +
                            $"\n{message}", LogType.Error);
            }

            return result;
        }

        public void SetAuthToken(Token token)
        {
            _token = token;
            _client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token.accessToken);
        }

        public Token GetAuthToken() => _token;

        private void OnDestroy()
        {
            _client?.Dispose();
        }

        private void OnApplicationQuit()
        {
            _client?.Dispose();
        }

        public static NetworkManager Instance => _instance;
        public NetworkConfig Config => _config;
        public NetworkImageLoader NetworkImageLoader => _networkImageLoader;
        public ILogger Logger => _logger;
    }
}