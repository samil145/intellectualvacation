﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace IO
{
    public class LocalStorage : MonoBehaviour
    {
        private static LocalStorage _instance;
        private readonly IDictionary<string, object> _data = new Dictionary<string, object>();

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }

        public void Set(string key, object obj)
        {
            _data[key] = obj;
        }

        public T Get<T>(string key)
        {
            T value = default;

            if (_data.TryGetValue(key, out var rawData))
            {
                if (rawData is T converted)
                {
                    value = converted;
                }
            }

            return value;
        }

        public static LocalStorage Instance => _instance;
    }
}