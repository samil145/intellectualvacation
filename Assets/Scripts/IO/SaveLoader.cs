﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using System.IO;
using System.Linq;

namespace IO
{
    public class SaveLoader : MonoBehaviour
    {
        private static SaveLoader _instance;
        private SavedData _savedData;

        private async void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                await LoadListOfInterest();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private async Task LoadListOfInterest()
        {
            string dataPath = Application.dataPath;

            using (FileStream stream = File.Open(dataPath + "/data.json", FileMode.OpenOrCreate))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string json = await reader.ReadToEndAsync();
                    _savedData = JsonUtility.FromJson<SavedData>(json) ?? new SavedData();
                }
            }
        }

        private async void OnApplicationQuit()
        {
            await SaveListOfInterest();
        }

        private async Task SaveListOfInterest()
        {
            if (_savedData != null)
            {
                string dataPath = Application.dataPath;
                await using (Stream stream = File.Create(dataPath + "/data.json"))
                {
                    await using (StreamWriter writer = new StreamWriter(stream))
                    {
                        string json = JsonUtility.ToJson(_savedData);
                        await writer.WriteAsync(json);
                    }
                }
            }
        }

        public List<ulong> ListOfInterest
        {
            get => _savedData.listOfInterests;
            set => _savedData.listOfInterests = value;
        }

        public static SaveLoader Instance => _instance;
    }
}