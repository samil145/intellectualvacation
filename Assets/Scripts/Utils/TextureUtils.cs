﻿using UnityEngine;

namespace Utils
{
    public static class TextureUtils
    {
        public static Texture2D Constrain(Texture2D original, RectInt bounds)
        {
            var constrainedTexture = new Texture2D(bounds.width, bounds.height);

            RenderTexture renderTexture = RenderTexture.GetTemporary(original.width, original.height, 0,
                RenderTextureFormat.Default, RenderTextureReadWrite.Default);
            Graphics.Blit(original, renderTexture);

            Graphics.CopyTexture(renderTexture, 0, 0, bounds.x, bounds.y, bounds.width, bounds.height, 
                constrainedTexture, 0, 0, 0, 0);

            RenderTexture.ReleaseTemporary(renderTexture);
            
            return constrainedTexture;
        }
        
        public static Texture2D DuplicateTexture(this Texture2D source)
        {
            RenderTexture renderTex = RenderTexture.GetTemporary(
                source.width,
                source.height,
                0,
                RenderTextureFormat.Default,
                RenderTextureReadWrite.Linear);

            Graphics.Blit(source, renderTex);
            RenderTexture previous = RenderTexture.active;
            RenderTexture.active = renderTex;
            Texture2D readableText = new Texture2D(source.width, source.height);
            readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
            readableText.Apply();
            RenderTexture.active = previous;
            RenderTexture.ReleaseTemporary(renderTex);
            return readableText;
        }
    }
}