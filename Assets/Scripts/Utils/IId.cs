﻿namespace Utils
{
    public interface IId
    {
        ulong ID { get; }
    }
}